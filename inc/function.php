<?php
	function query($qry) {
		$result = mysql_query($qry) or die("Gagal melakukan query pada :
	 	<br>$qry<br><br>Kode Salah : <br>&nbsp;&nbsp;&nbsp;" . mysql_error() . "!");
		return $result;
	}

	function get_bobot($a, $b, $c, $d, $e) {
		return (($a * 4) + ($b * 3) + ($c * 2) + ($d * 1) + ($e * 0)) / ($a + $b + $c + $d + $e);
	}

	function get_count_inbox($nim) {
		$sql = "select count(id_crm_event) from crm_inbox where nim='$nim'";
		return fetch_row($sql);
	}

	function cek_polling_aktif($id_jadwal) {
		$sql = "select count(id_jadwal) from crm_hasil_survey where id_jadwal='$id_jadwal' ";
		if (fetch_row($sql) > 0) :
			return true;
		else :
			return false;
		endif;	
	}
	
	function cek_sudah_polling($id_jadwal,$nim) {
		$sql = "select id_jadwal from crm_polling_status where id_jadwal='$id_jadwal' 
				and nim='$nim' ";	
		$result=mysql_query($sql);
		$ada=mysql_num_rows($result);

		if ($ada > 0) :
			return false;
		else :
			return true;
		endif;
	}

	function get_jumlah_soal() {
		return fetch_row("select count(id_kuisioner) from crm_kuisioner ");
	}

	function get_last_id_crm_event() {
		return fetch_row("select id_crm_event from crm_event order by id_crm_event desc limit 1 ");
	}

	function arrayToObject($array) {
		if (!is_array($array)) {
			return $array;
		}

		$object = new stdClass();
		if (is_array($array) && count($array) > 0) {
			foreach ($array as $name => $value) {
				$name = strtolower(trim($name));
				if (!empty($name)) {
					$object -> $name = arrayToObject($value);
				}
			}
			return $object;
		} else {
			return FALSE;
		}
	}

	function get_idkabupaten($lat, $lng) {
		$id = fetch_row("select idkabupaten from kabupaten 
			where lat='$lat' and lng='$lng'");
		return $id;
	}

	function fetch_row($qry) {
		$tmp = query($qry);
		list($result) = mysql_fetch_row($tmp);
		return $result;
	}

	function get_total($query) {
		$tmp = query($query);
		list($result) = mysql_fetch_row($tmp);
		return $result;
	}

	function cek_status_polling($idjadwal) {
		$tmp = query("select id_jadwal from crm_hasil_survey where id_jadwal='$idjadwal'");
		$hasil = mysql_num_rows($tmp);
		return $hasil;
	}

	function combo_kabupaten($kode) {
		echo "<option value='' selected>- Pilih kabupaten -</option>";
		$query = query("SELECT lat,lng,nama  FROM kabupaten");
		while ($k = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$k[0],$k[1]'> " . ucwords($k[2]) . "</option>";
			else
				echo "<option value='$k[0],$k[1]'> " . ucwords($k[2]) . "</option>";
		}
	}

	function combo_minat($kode) {
		echo "<option value='' selected>- Pilih  -</option>";
		$query = query("SELECT * from  crm_minat");
		while ($k = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$k[0]'> " . ucwords($k[1]) . "</option>";
			else
				echo "<option value='$k[0]'> " . ucwords($k[1]) . "</option>";
		}
	}

	function combo_kategori($kode) {
		echo "<option value='' selected>- Pilih kategori -</option>";
		$query = query("SELECT *  FROM kategori");
		while ($row = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$row[0]'> " . ucwords($row[1]) . " </option>";
			else
				echo "<option value='$row[0]'" . selected($row[0], $kode) . "> " . ucwords($row[1]) . " </option>";
		}
	}

	function combo_jurusan($kode) {
		echo "<option value='' selected>- Pilih jurusan -</option>";
		$query = query("SELECT kode_jurusan,nama_jurusan  FROM Jurusan");
		while ($row = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$row[0]'> " . ucwords($row[1]) . " </option>";
			else
				echo "<option value='$row[0]'" . selected($row[0], $kode) . "> " . ucwords($row[1]) . " </option>";
		}
	}

	function combo_agama($kode) {
		echo "<option value='' selected>- Pilih agama -</option>";
		$query = query("SELECT agama_ID,nama  FROM agama");
		while ($row = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$row[0]'> " . ucwords($row[1]) . " </option>";
			else
				echo "<option value='$row[0]'" . selected($row[0], $kode) . "> " . ucwords($row[1]) . " </option>";
		}
	}

	function combo_crm_kategori($kode) {
		echo "<option value='' selected>- Pilih kategori -</option>";
		$query = query("SELECT *  FROM crm_kategori");
		while ($row = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$row[0]'> " . ucwords($row[1]) . " </option>";
			else
				echo "<option value='$row[0]'" . selected($row[0], $kode) . "> " . ucwords($row[1]) . " </option>";
		}
	}

	function combo_crm_kategori2($kode) {
		echo "<option value='' selected>- Pilih kategori -</option>";
		$query = query("SELECT *  FROM crm_kategori where id_crm_kategori>1");
		while ($row = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$row[0]'> " . ucwords($row[1]) . " </option>";
			else
				echo "<option value='$row[0]'" . selected($row[0], $kode) . "> " . ucwords($row[1]) . " </option>";
		}
	}

	function combo_crm_minat($kode) {
		echo "<option value='' selected>- Pilih minat -</option>";
		$query = query("SELECT *  FROM crm_minat");
		while ($row = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$row[0]'> " . ucwords($row[1]) . " </option>";
			else
				echo "<option value='$row[0]'" . selected($row[0], $kode) . "> " . ucwords($row[1]) . " </option>";
		}
	}

	function get_today() {
		$today = date("Y-m-d");
		return $today;
	}

	function format_rupiah($rp) {
		$hasil = "<b>Rp." . number_format($rp, 0, "", ".") . ",00</b>";
		return $hasil;
	}

	function num_rows($qry) {
		$tmp = query($qry);
		$jum = mysql_num_rows($tmp);
		return $jum;
	}

	function valid($tmp) {
		return htmlentities(addslashes($tmp));
	}

	//fungsi untuk meremove koma didepan dan dibelakang
	function rm_koma($data) {
		$ret = substr($data, 0, -1);	
		return $ret;
	}

	function combo_hari($kode) {
		echo "<option value='0' selected>-  hari -</option>";
		$hari = array('senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu');
		foreach ($hari as $value) {
			if ($kode == "")
				echo "<option value='$value'> " . ucwords($value) . " </option>";
			else
				echo "<option value='$value'" . selected($value, $kode) . "> " . ucwords($value) . " </option>";
		}
	}

	function konversi_bulan($bln) {
		switch($bln) {
			case "1" :
			case "01" :
				$bulan = "Januari";
				break;
			case "2" :
			case "02" :
				$bulan = "Februari";
				break;
			case "3" :
			case "03" :
				$bulan = "Maret";
				break;
			case "4" :
			case "04" :
				$bulan = "April";
				break;
			case "5" :
			case "05" :
				$bulan = "Mei";
				break;
			case "6" :

			case "06" :
				$bulan = "Juni";
				break;
			case "7" :

			case "07" :
				$bulan = "Juli";
				break;
			case "8" :

			case "08" :
				$bulan = "Agustus";
				break;
			case "9" :

			case "09" :
				$bulan = "September";
				break;
			case "10" :
				$bulan = "Oktober";
				break;
			case "11" :
				$bulan = "November";
				break;
			case "12" :
				$bulan = "Desember";
				break;
			default :
				$bulan = "Nooooooot..!!";
		}
		return $bulan;
	}

	function konversi_tanggal($time) {
		list($thn, $bln, $tgl) = explode('-', $time);
		$tmp = $tgl . " " . konversi_bulan($bln) . " " . $thn;
		return $tmp;
	}

	function tampil_tanggal($time) {
		list($date, $time) = explode(' ', $time);
		$tmp = konversi_tanggal($date) . " " . $time;
		return $tmp;
	}

	function selected($t1, $t2) {
		if (trim($t1) == trim($t2))
			return "selected";
		else
			return "";
	}

	function get_date($tgl = '') {
		if ($tgl == "")
			$now = date("d");
		else
			$now = $tgl;
			$jum_hr = date("t");
		for ($i = 1; $i <= $jum_hr; $i++) {
			echo "<option value='$i' " . selected($i, $now) . ">$i</option>";
		}
	}

	function get_month($bln = '') {
		if ($bln == "")
			$now = date("m");
		else
			$now = $bln;
			$jum_bl = 12;
		for ($i = 1; $i <= $jum_bl; $i++) {
			echo "<option value='$i' " . selected($i, $now) . ">" . konversi_bulan($i) . "</option>";
		}
	}

	function get_year($thn = '') {
		if ($thn == "") {
			$now = date("Y");
			$thn = date("Y");
		} else {
			$now = date("Y");
			$thn = $thn;
		}
		$jum_th = 3;
		for ($i = 1; $i <= $jum_th; $i++) {
			echo "<option value='$now' " . selected($thn, $now) . ">" . $now . "</option>";
			$now--;
		}
	}

	function combo_tabel($kode,$tabel,$label) {
		echo "<option value=''> " . $label . " </option>";
		$query = query("SELECT *   FROM ".$tabel);
		while ($row = mysql_fetch_row($query)) {
			if ($kode == "")
				echo "<option value='$row[0]'> " . ucwords($row[1]) . " </option>";
			else
				echo "<option value='$row[0]'" . selected($row[0], $kode) . "> " . ucwords($row[1]) . " </option>";
		}
	}

	function combo_tahun_angkatan(){
		echo "<option value='00'>All</option>";
		echo "<option value='14'>2014</option>";
		echo "<option value='13'>2013</option>";
		echo "<option value='12'>2012</option>";
		echo "<option value='11'>2011</option>";
		echo "<option value='10'>2010</option>";
		echo "<option value='09'>2009</option>";
		echo "<option value='08'>2008</option>";
	}
?>
