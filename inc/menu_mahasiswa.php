<div class="liner"></div> 
<li>
	<a href="index.php?m=login&pg=welcome" data-toggle="tooltip" title="Home">
		<span class="round-tabs">
			<i class="glyphicon glyphicon-home"><br/><p class="font-light">HOME</p></i>
		</span>
	</a>
</li>
<li>
	<a href="index.php?m=mahasiswa&pg=mahasiswa_detail" data-toggle="tooltip" title="Profil">
		<span class="round-tabs">
			<i class="glyphicon glyphicon-user"><br/><p class="font-light">PROFIL</p></i>
		</span>
	</a>
</li>
<li>
	<a href="index.php?m=matakuliah&pg=matakuliah_mview" data-toggle="tooltip" title="Matakuliah">
		<span class="round-tabs">
			<i class="glyphicon glyphicon-list"><br/><p class="font-light">MATAKULIAH</p></i>
		</span> </a>
</li>
<li>
	<a href="index.php?m=mahasiswa&pg=mahasiswa_transkrip"data-toggle="tooltip" title="Transkrip">
		<span class="round-tabs">
			<i class="glyphicon glyphicon-file"><br/><p class="font-light">TRANSKRIP</p></i>
		</span>
	</a>
</li>                               
<li style="margin-left: 25%">
	<a href="index.php?m=crm_evaluasi&pg=crm_evaluasi_mview" data-toggle="tooltip" title="Kritik Saran">
		<span class="round-tabs">
			<i class="glyphicon glyphicon-edit"><br/><p class="font-light">KRITIK SARAN</p></i>
		</span>
	</a>
</li>
<li>
	<a href="index.php?m=crm_event&pg=crm_event_inbox" data-toggle="tooltip" title="Kotak Pesan">
		<span class="round-tabs">                      
			<i class="glyphicon glyphicon-envelope"><br/><p class="font-light">KOTAK PESAN(<?php
				$nim=$_SESSION['username']; 
				echo get_count_inbox($nim)?>) </p></i>
		</span>
	</a>
</li>