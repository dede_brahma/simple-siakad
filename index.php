<?php session_start();
	error_reporting(0);
	$username = $_SESSION['username'];
	if (empty($username))
	{
    	header('Location:login.php');
    	exit();
	}
	include ('inc/config.php');
	include ('inc/function.php');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title> Admin CRM</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link  href='assets/css/thema.css' rel='stylesheet'>
		<script type='text/javascript' src="assets/js/jquery.js"></script>
		<script type='text/javascript' src="assets/js/bootstrap.min.js"></script>
		<script type='text/javascript' src="assets/js/jquery.validate.js"></script>
		<script type='text/javascript' src="assets/js/messages_id.js"></script>
		<script type='text/javascript' src="assets/js/notify.min.js"></script>
		<!-- JavaScript jQuery code from Bootply.com editor -->

		<script type='text/javascript'>
			$(document).ready(function()
			{
				$(".alert").addClass("in").fadeOut(4500);
				/* swap open/close side menu icons */
				$('[data-toggle=collapse]').click(function() 
				{
					// toggle icon
					$(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
				});
			});
		</script>
		
		<script type="text/javascript">
			$(document).ready(function()
			{
    			$('[data-toggle="tooltip"]').tooltip(
    			{
        			placement : 'bottom'
    			});
			});
		</script>

		<script>
			$(document).ready(function()
			{
      			$('body').append('<div id="toTop" class="btn btn-danger"><span class="glyphicon glyphicon-chevron-up"></span></div>');
    			$(window).scroll(function () {
				if ($(this).scrollTop() != 0) {
					$('#toTop').fadeIn();
				} else {
					$('#toTop').fadeOut();
					}
				}); 
    			$('#toTop').click(function(){
        			$("html, body").animate({ scrollTop: 0 }, 600);
       				 return false;
    			});
			});
		</script>

		<?php if($_SESSION['level']=='mahasiswa'): ?>	
			<script src="//js.pusher.com/2.2/pusher.min.js" type="text/javascript"></script>
  			<script type="text/javascript">
  				$(document).ready(function() {
    				// Enable pusher logging - don't include this in production
    				Pusher.log = function(message) {
      					if (window.console && window.console.log) {
        					window.console.log(message);
      					}
    				};	
					//ganti degan APPKEY sesuai dengan yagn ada di infoAction 	
    				var pusher = new Pusher('3cf5a7bc3708fed04f8f');
    				var channel = pusher.subscribe('test_channel');
   					channel.bind('my_event', function(data) {
     					// alert(data.message);
      					$.notify(data.message, "success", {
							position : "top right",
							autoHideDelay: 120000
						});
    				});
     			});
    		</script>
		<?php endif;?>

		<!-- JavaScript jQuery code from Bootply.com editor -->
		<style type="text/css">
			label.error {
				color: red;
			}
			body{
			background-image:url('assets/img/shattered.png');
			background-repeat:repeat;
			}
		</style>
		<script>
			$(document).ready(function() {
				$("#form1").validate();
			});
		</script>
		<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!-- CSS code from Bootply.com editor -->
	</head>

	<!-- HTML code from Bootply.com editor -->
	<body>
	<!-- Header -->
	<div class="container">			
		<div class="header">
			<img src="assets/img/akakom.ac.id-logo.png"
        		class="img-responsive" align="left" width='250px'>
			<a href="login/logout.php"><img src="assets/img/logout.png"
            	class="img-responsive" align="right" width='70px'/></a>
		</div>					
	</div>
	<!-- /Header -->

	<!-- Main -->
	<div class="container">
		<div class="row">				
			<div class="board">
				<div class="board-inner" style="background: url(assets/img/restaurant.png)">
					<ul class="nav nav-tabs" id="myTab">
                    <?php
						if($_SESSION['level']=='pengelola'){
							include('inc/menu_admin.php');								
							}else if($_SESSION['level']=='dosen'){
								include('inc/menu_dosen.php');								
							}else if($_SESSION['level']=='mahasiswa'){
								include('inc/menu_mahasiswa.php');
							}
					?>								
					</ul>
				</div>
				<div class="tab-content">
					<?php
                    	//include('inc/config.php');
                    	if (!isset($_GET['pg'])) {
                        	include ('crm_admin/crm_admin_view.php');
                    	} else {
                        	$pg = $_GET['pg'];
                        	$mod = $_GET['m'];
                        	include $mod . '/' . $pg . ".php";
                    	}
					?>
				</div><!--/col-span-9-->
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- /Main -->

	<div class="container">
		<div class="row">
			<div class="footer">
				<p>APLIKASI CRM 2015</p>
			</div>			
		</div>
	</div>

	</body>
</html>