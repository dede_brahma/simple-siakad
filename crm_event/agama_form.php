<?php 
	$aksi = 'tambah';
?>         

<div class="tab-content">
	<div class="tab-pane fade in active">
		<h3 class="head text-center">Notifikasi keagamaan</h3> 
        <form  id='form1' method="POST" 
               action='crm_event/agama_action.php'
               class="form-horizontal" role="form"
               enctype="multipart/form-data">
                     
				<div class="form-group">
    				<label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>
    				<div class="col-sm-6">
      					<input type="date" class="form-control required " id="tanggal" name='tanggal'>
    				</div>
				</div>
 				<div class="form-group">
    				<label for="inputEmail3" class="col-sm-3 control-label">Agama</label>
    				<div class="col-sm-6" >
      					<select name='kategori' class='form-control'>
      						<?php
      							combo_agama(null);
							?>
      					</select>
    				</div>
      			</div>
      			<div class="form-group">
    				<label for="inputEmail3" class="col-sm-3 control-label">Isi</label>
    				<div class="col-sm-6" >
      					<textarea name='isi' class='form-control required' rows='5'>
      						<?php
      							echo $crm_event->isi;
							?>
     	 				</textarea>
    				</div>
      			</div>
				<div class="form-group">
    				<div class="col-sm-offset-3 col-sm-8">
      					<button type="submit" class="btn btn-primary" name='aksi' value='<?=$aksi?>'><?=$aksi?></button>
      					<a href='index.php?m=crm_event&pg=crm_event_view' class='btn btn-danger'>Cancel</a>
    				</div>
  				</div>
		</form>
	</div><!--/panel content-->
</div><!--/panel-->
                    

                