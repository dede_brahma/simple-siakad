<?php 
	$nim = $_GET['id'];
	if(empty($nim)){
		$nim=$_SESSION['username'];
	}
?>

<div class="tab-content">
	<div class="tab-pane fade in active">
		<h3 class="head text-center">Biodata Mahasiswa</h3>  
		<?php $query = "select mahasiswa.*,jurusan.nama_jurusan,agama.nama,
						dosen.nama_lengkap as nama_dosen
 						from mahasiswa,jurusan,agama,dosen
 						where mahasiswa.kode_jurusan=jurusan.kode_jurusan
 						and mahasiswa.Agama=agama.agama_ID
 						and mahasiswa.PenasehatAkademik=dosen.NIDN
						and mahasiswa.NIM='$nim'";

			$result = mysql_query($query) or die(mysql_error());
			$rows = mysql_fetch_object($result);
		?>
		<div class="table-responsive">
			<table  class="table table-condensed">
				<tbody>
					<tr>		
						<td width="200px"><b>Nama</b></td>
						<td>:</td>
						<td><? echo $rows -> Nama; ?></td>
						<td rowspan='9'><img src='upload/foto/foto.jpg' width='200px' height='200px'/></td>
					</tr>		
					<tr>		
						<td width="200px"><b>NIM</b></td>
						<td>:</td>
						<td><? echo $rows -> NIM; ?></td>
					</tr>
					<tr>		
						<td width="200px"><b>Jurusan</b></td>
						<td>:</td>
						<td><? echo $rows ->nama_jurusan; ?></td>			
					</tr>
					<tr>		
						<td width="200px"><b>Pembimbing</b></td>
						<td>:</td>
						<td><? echo $rows ->nama_dosen; ?></td>			
					</tr>
					<tr>		
						<td width="200px"><b>TTL</b></td>
						<td>:</td>
						<td><? echo $rows ->TempatLahir; ?>/
							<? echo tampil_tanggal($rows ->TanggalLahir); ?></td>			
					</tr>
					<tr>		
						<td width="200px"><b>Jenis Kelamin</b></td>
						<td>:</td>
						<td><?php echo $rows->Kelamin ?></td>			
					</tr>
					<tr>		
						<td width="200px"><b>Agama</b></td>
						<td>:</td>
						<td><? echo $rows ->nama ?></td>			
					</tr>
			</table>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"style='text-align: center;margin-bottom:0px;'>Transkrip Nilai</h3>
			</div>
			<div class="panel-body">
				<?php
					$nim = $_GET['id'];
					if(empty($nim)){
						$nim=$_SESSION['username'];
					}
					$sql="SELECT * FROM view_form_mhsakademik WHERE NIM='$nim'";
					$no=0;
					$qry=mysql_query($sql)
						or die ();
					$ab=mysql_num_rows($qry);
					$data=mysql_fetch_array($qry);
					$no++;
				?>
			<div class='table-responsive'>
				<?php echo" 
				<table class='table table-bordered'>
						<tr>
							<th>NO</th><th>Kode Mtk</th>
							<th>Nama Mtk</th><th>SKS</th><th>Sem</th>
							<th>Grade</th><th>Bobot</th><th>Nilai</th>
						</tr>";
						$sql="SELECT * FROM matakuliah WHERE Jurusan_ID='$data[kode_jurusan]' ORDER BY semester,kode_mtk";
 						$no=0;
						$qry=mysql_query($sql)
							or die ();
						while($data=mysql_fetch_array($qry)){
							$no++;
							$sqlr="SELECT * FROM view_ipk  WHERE  NIM='$nim' AND kode_mtk='$data[Kode_mtk]'";
							$qryr= mysql_query($sqlr);
							$data1=mysql_fetch_array($qryr);
							echo" 
							<tr>
								<td align=center>$no</td>
            					<td bgcolor=#ececec>$data[Kode_mtk]</td>
            					<td>$data[Nama_matakuliah]</td>
								<td align=center bgcolor=#ececec>$data[SKS]</td>
								<td align=center>$data[Semester]</td>
								<td align=center bgcolor=#ececec>$data1[GradeNilai]</td>
								<td align=center>$data1[BobotNilai]</td>";
								$boboxtsks=$data1[SKS]*$data1[BobotNilai];
								echo" 
									<td align=center bgcolor=#ececec>$boboxtsks</td>";
								$TotSks=$TotSks+$data[SKS];
								$Tot=$Tot+$data[SKS];
								$jmlbobot=$jmlbobot+$boboxtsks;
								echo" 
							</tr>";   	  
  						}
						echo" 
							<td colspan=3 align=right></td>
							<td colspan=1 align=center bgcolor=#ececec>";
						echo number_format($Tot,0,',','.');
						echo"</td>
							<td colspan=3 align=right></td>
        					<td colspan=1 align=center bgcolor=#ececec>";
  						echo number_format($jmlbobot,0,',','.');
						echo" </td>
				</table>";
				$ipk=$jmlbobot/$Tot;
				echo" 
				<table id=tablemodul2>
					<tr>
						<th>Total Keseluruhan SKS  =</th>
							<td class=cb><strong>";
								echo number_format($Tot,0,',','.');
								echo"</tr>";
								echo" <tr><th>Index Prestasi Kumulatif = 
						</th> 
          				<td class=cb><strong>";
          					echo number_format($ipk,2,',','.');
							echo" </strong></td></tr></table></div>";
  				?>
			</div>
		</div>
			<a href='index.php?m=mahasiswa&pg=mahasiswa_view1' class='btn btn-primary'>Kembali</a>
		</div>
</div>