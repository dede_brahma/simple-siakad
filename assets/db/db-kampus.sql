-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 08. Juni 2015 jam 07:46
-- Versi Server: 5.5.16
-- Versi PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db-kampus`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(1) NOT NULL,
  `username` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `keterangan` text COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `telepon` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `id_level`, `username`, `password`, `keterangan`, `nama_lengkap`, `email`, `telepon`, `aktif`) VALUES
(1, 1, 'biondi', '39b099448d87f16ef848fd4e6381a06a', 'Web Developer', 'Biondi Jourdan', 'biondif@gmail.com', '-', 'Y'),
(2, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Web Developer', 'Roki Aditama', 'roro.roki@gmail.com', '-', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `agama`
--

CREATE TABLE IF NOT EXISTS `agama` (
  `agama_ID` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`agama_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `agama`
--

INSERT INTO `agama` (`agama_ID`, `nama`, `aktif`) VALUES
(1, 'ISLAM', 'Y'),
(2, 'KRISTEN KATOLIK', 'Y'),
(3, 'KRISTEN PROTESTAN', 'Y'),
(4, 'BUDHA', 'Y'),
(5, 'HINDU', 'Y'),
(6, 'LAIN-LAIN', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beritaawal`
--

CREATE TABLE IF NOT EXISTS `beritaawal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `isi` text COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `beritaawal`
--

INSERT INTO `beritaawal` (`id`, `tanggal`, `isi`, `gambar`, `aktif`) VALUES
(1, '2011-02-25', 'Pembuatan Aplikasi Sistem Informasi Akademik Kampus ini bertujuan sebagai bahan pembelajaran bagi para programmer di indonesia baik tingkat pemula, menegah,maupun mahir, sehingga dengan hadirnya pembelajaran ini kedepannya para programmer dapat membuat berbagai aplikasi lainnya dengan memahami konsep sederhana yang penulis sajikan ini \r\n\r\ndan didalam praktek pembuatan aplikasi tersebut jika para programmer sulit memahami jalannya program untuk segera merujuk kepada buku yang telah diterbitkan\r\n\r\n', 'icon.png', 'Y');

-- --------------------------------------------------------

--
-- Stand-in structure for view `cetakmtk`
--
CREATE TABLE IF NOT EXISTS `cetakmtk` (
`Matakuliah_ID` int(11)
,`Identitas_ID` varchar(100)
,`Kode_mtk` varchar(15)
,`Nama_matakuliah` varchar(100)
,`Semester` varchar(2)
,`SKS` varchar(2)
,`Jurusan_ID` varchar(10)
,`JenisMTK_ID` varchar(2)
,`JenisKurikulum_ID` varchar(2)
,`StatusMtk_ID` varchar(20)
,`Penanggungjawab` varchar(50)
,`Aktif` enum('Y','N')
,`NamaSMk` varchar(20)
,`NamaJMK` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `coba`
--
CREATE TABLE IF NOT EXISTS `coba` (
`Identitas_ID` varchar(10)
,`Nama_Identitas` varchar(100)
,`kode_jurusan` varchar(11)
,`nama_jurusan` varchar(100)
,`jenjang` varchar(5)
);
-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_admin`
--

CREATE TABLE IF NOT EXISTS `crm_admin` (
  `id_crm_admin` int(10) NOT NULL AUTO_INCREMENT,
  `nama_crm_admin` varchar(50) NOT NULL,
  `id_crm_kategori` int(1) NOT NULL,
  `username` varchar(8) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_crm_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `crm_admin`
--

INSERT INTO `crm_admin` (`id_crm_admin`, `nama_crm_admin`, `id_crm_kategori`, `username`, `password`) VALUES
(1, 'dede brahma', 1, 'dede', 'b4be1c568a6dc02dcaf2849852bdb13e'),
(3, 'Sudarmanto', 2, 'sudarman', '96b42d8f3c7a7a9348b39936193ba983'),
(4, 'Sri rejeki', 4, 'sri', 'd1565ebd8247bbb01472f80e24ad29b6'),
(5, 'Bertha', 3, 'bertha', '3d63c18129e097fe5fca9ba71cfaad03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_evaluasi`
--

CREATE TABLE IF NOT EXISTS `crm_evaluasi` (
  `id_crm_evaluasi` int(10) NOT NULL AUTO_INCREMENT,
  `id_crm_kategori` int(10) NOT NULL,
  `isi_evaluasi` varchar(50) NOT NULL,
  `status_baca` varchar(10) NOT NULL DEFAULT 'belum',
  `id_crm_admin` int(10) NOT NULL,
  `nim` varchar(11) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_crm_evaluasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `crm_evaluasi`
--

INSERT INTO `crm_evaluasi` (`id_crm_evaluasi`, `id_crm_kategori`, `isi_evaluasi`, `status_baca`, `id_crm_admin`, `nim`, `tanggal`) VALUES
(2, 2, 'KRS kok eror tereus ya?', 'sudah', 0, '20102611001', '2014-10-08'),
(3, 4, 'kapan ada acara seminar android?', 'sudah', 0, '20102611001', '2014-10-08'),
(4, 3, 'mohon untuk pembenahan AC di ruang U.2.3', 'sudah', 0, '105410201', '2015-01-21'),
(5, 2, 'test', 'sudah', 0, '115410206', '2015-02-17'),
(6, 2, 'test 26 april 2015', 'sudah', 0, '115410206', '2015-04-26'),
(7, 3, 'test 2\r\n26 april 2015\r\nkategori kampus', 'sudah', 0, '115410206', '2015-04-26'),
(8, 3, 'test  terbaru', 'belum', 0, '115410206', '2015-05-08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_event`
--

CREATE TABLE IF NOT EXISTS `crm_event` (
  `id_crm_event` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `isi` text NOT NULL,
  `kategori` varchar(30) NOT NULL,
  PRIMARY KEY (`id_crm_event`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `crm_event`
--

INSERT INTO `crm_event` (`id_crm_event`, `tanggal`, `isi`, `kategori`) VALUES
(3, '2015-01-17', 'selamat ulang tahun!', 'ultah'),
(4, '2015-01-20', 'selamat ulang tahun!', 'ultah'),
(7, '2015-01-21', 'selamat ulang tahun!', 'ultah'),
(11, '2015-02-21', 'besok libur', 'info'),
(12, '2015-02-21', '      	      besok libur', 'info'),
(13, '2015-03-14', 'selamat ulang tahun!', 'ultah'),
(14, '2015-04-26', 'selamat ulang tahun!', 'ultah'),
(15, '2015-04-26', '      	      test hari ini', 'info'),
(16, '2015-05-05', '      	  test terbaru    ', 'info');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_hasil_survey`
--

CREATE TABLE IF NOT EXISTS `crm_hasil_survey` (
  `id_survey` int(10) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(10) NOT NULL,
  `no_quisioner` int(2) NOT NULL,
  `A` int(2) NOT NULL DEFAULT '0',
  `B` int(2) NOT NULL DEFAULT '0',
  `C` int(2) NOT NULL DEFAULT '0',
  `D` int(2) NOT NULL DEFAULT '0',
  `E` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_survey`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `crm_hasil_survey`
--

INSERT INTO `crm_hasil_survey` (`id_survey`, `id_jadwal`, `no_quisioner`, `A`, `B`, `C`, `D`, `E`) VALUES
(1, 83, 1, 1, 1, 0, 0, 0),
(2, 83, 2, 2, 0, 0, 0, 0),
(3, 83, 3, 0, 1, 1, 0, 0),
(4, 83, 4, 1, 1, 0, 0, 0),
(5, 83, 5, 0, 1, 0, 0, 1),
(6, 83, 6, 1, 0, 0, 1, 0),
(7, 83, 7, 0, 0, 1, 1, 0),
(8, 83, 8, 0, 2, 0, 0, 0),
(9, 83, 9, 0, 0, 1, 1, 0),
(10, 83, 10, 0, 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_inbox`
--

CREATE TABLE IF NOT EXISTS `crm_inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(9) NOT NULL,
  `id_crm_event` int(11) NOT NULL,
  `status_baca` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=195 ;

--
-- Dumping data untuk tabel `crm_inbox`
--

INSERT INTO `crm_inbox` (`id`, `nim`, `id_crm_event`, `status_baca`) VALUES
(1, '125410101', 3, 0),
(26, '115410206', 7, 0),
(99, '105410201', 11, 0),
(100, '105410203', 11, 0),
(101, '105410239', 11, 0),
(102, '105410254', 11, 0),
(103, '105410268', 11, 0),
(104, '115410197', 11, 0),
(105, '115410206', 11, 0),
(106, '115410224', 11, 0),
(107, '115410297', 11, 0),
(108, '125410078', 11, 0),
(109, '125410101', 11, 0),
(110, '125410196', 11, 0),
(111, '125410200', 11, 0),
(112, '125410267', 11, 0),
(113, '125610075', 11, 0),
(114, '125810084', 11, 0),
(115, '135410006', 11, 0),
(116, '135410128', 11, 0),
(117, '135410206', 11, 0),
(118, '135410261', 11, 0),
(119, '135411303', 11, 0),
(120, '135412209', 11, 0),
(121, '135610012', 11, 0),
(122, '135810072', 11, 0),
(123, '105410201', 12, 0),
(124, '105410203', 12, 0),
(125, '105410239', 12, 0),
(126, '105410254', 12, 0),
(127, '105410268', 12, 0),
(128, '115410197', 12, 0),
(129, '115410206', 12, 0),
(130, '115410224', 12, 0),
(131, '115410297', 12, 0),
(132, '125410078', 12, 0),
(133, '125410101', 12, 0),
(134, '125410196', 12, 0),
(135, '125410200', 12, 0),
(136, '125410267', 12, 0),
(137, '125610075', 12, 0),
(138, '125810084', 12, 0),
(139, '135410006', 12, 0),
(140, '135410128', 12, 0),
(141, '135410206', 12, 0),
(142, '135410261', 12, 0),
(143, '135411303', 12, 0),
(144, '135412209', 12, 0),
(145, '135610012', 12, 0),
(146, '135810072', 12, 0),
(147, '105410201', 15, 0),
(148, '105410203', 15, 0),
(149, '105410239', 15, 0),
(150, '105410254', 15, 0),
(151, '105410268', 15, 0),
(152, '115410197', 15, 0),
(153, '115410206', 15, 0),
(154, '115410224', 15, 0),
(155, '115410297', 15, 0),
(156, '125410078', 15, 0),
(157, '125410101', 15, 0),
(158, '125410196', 15, 0),
(159, '125410200', 15, 0),
(160, '125410267', 15, 0),
(161, '125610075', 15, 0),
(162, '125810084', 15, 0),
(163, '135410006', 15, 0),
(164, '135410128', 15, 0),
(165, '135410206', 15, 0),
(166, '135410261', 15, 0),
(167, '135411303', 15, 0),
(168, '135412209', 15, 0),
(169, '135610012', 15, 0),
(170, '135810072', 15, 0),
(171, '105410201', 16, 0),
(172, '105410203', 16, 0),
(173, '105410239', 16, 0),
(174, '105410254', 16, 0),
(175, '105410268', 16, 0),
(176, '115410197', 16, 0),
(177, '115410206', 16, 0),
(178, '115410224', 16, 0),
(179, '115410297', 16, 0),
(180, '125410078', 16, 0),
(181, '125410101', 16, 0),
(182, '125410196', 16, 0),
(183, '125410200', 16, 0),
(184, '125410267', 16, 0),
(185, '125610075', 16, 0),
(186, '125810084', 16, 0),
(187, '135410006', 16, 0),
(188, '135410128', 16, 0),
(189, '135410206', 16, 0),
(190, '135410261', 16, 0),
(191, '135411303', 16, 0),
(192, '135412209', 16, 0),
(193, '135610012', 16, 0),
(194, '135810072', 16, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_kategori`
--

CREATE TABLE IF NOT EXISTS `crm_kategori` (
  `id_crm_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id_crm_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `crm_kategori`
--

INSERT INTO `crm_kategori` (`id_crm_kategori`, `nama`) VALUES
(1, 'admin'),
(2, 'akademik'),
(3, 'Kampus'),
(4, 'kemahasiswaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_kuisioner`
--

CREATE TABLE IF NOT EXISTS `crm_kuisioner` (
  `id_kuisioner` int(10) NOT NULL AUTO_INCREMENT,
  `isi_pertanyaan` varchar(150) NOT NULL,
  `A` varchar(30) NOT NULL,
  `B` varchar(30) NOT NULL,
  `C` varchar(30) NOT NULL,
  `D` varchar(30) NOT NULL,
  `E` varchar(30) NOT NULL,
  PRIMARY KEY (`id_kuisioner`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `crm_kuisioner`
--

INSERT INTO `crm_kuisioner` (`id_kuisioner`, `isi_pertanyaan`, `A`, `B`, `C`, `D`, `E`) VALUES
(1, '         Dosen hadir sesuai jadwal?                   ', 'Sangat tepat waktu ', 'tepat waktu', 'Cukup tepat waktu ', 'kurang tepat waktu ', ' tidak tepat waktu'),
(2, 'Apakah kuliah sesuai silabus?              ', 'Sangat sesuai', 'sesuai ', 'cukup sesuai', 'Kurang sesuai', 'Tidak sesuai'),
(3, 'Apakah cara dosen menyampaikan kuliah secara jelas/mudah diterima?', 'Sangat jelas', 'Jelas', 'Cukup jelas', 'Kurang jelas', 'Tidak jelas'),
(4, 'Apakah dosen menguasai materi perkuliahan?', 'Sangat menguasai', 'Menguasai', 'Cukup menguasai', 'Kurang menguasai', 'Tidak menguasai'),
(5, 'Apakah buku pendukung materi mudah diperoleh?', 'Sangat mudah diperoleh', 'Mudah diperoleh', 'Cukup mudah diperoleh', 'Kurang mudah diperoleh', 'Tidak mudah diperoleh'),
(6, '              Apakah dosen memberikan motivasi belajar dan wasan baru didalam perkuliahan?', 'Sangat memberikan motivasi', 'Mmberikan motivasi', 'Cukup memberikan motivasi', 'Kurang memberikan motivasi', 'Tidak memberikan motivasi'),
(7, 'Apakah dosen memberikan kuis/tugas/pekerjaan rumah dalam perkuliahan?   ', 'Sangat sering', 'Sering', 'Cukup sering', 'Kurang', 'Tidak pernah'),
(8, '              Apakah dosen merangsang daya pikir/berpikir kritis mahasiswa?', 'Sangat merangsang daya pikir', 'Merangsang daya pikir', 'Cukup merangsang daya pikir', 'Kurang merangsang daya pikir', 'Tidak merangsang daya pikir'),
(9, '              Apakah soal ujian sesuai dengan materi perkuliahan?', 'Sangat sesuai', 'Sesuai', 'Cukup sesuai', 'Kurang sesuai', 'Tidak sesuai'),
(10, '              Apakah dosen transparansi/terbuka dalam penilaian?', 'Sangat transparan', 'Transparan', 'Cukup transparan', 'Kurang transparan', 'Tidak transparan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_mahasiswa_minat`
--

CREATE TABLE IF NOT EXISTS `crm_mahasiswa_minat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(9) NOT NULL,
  `id_minat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `crm_mahasiswa_minat`
--

INSERT INTO `crm_mahasiswa_minat` (`id`, `nim`, `id_minat`) VALUES
(5, '115410206', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_minat`
--

CREATE TABLE IF NOT EXISTS `crm_minat` (
  `id_minat` int(10) NOT NULL AUTO_INCREMENT,
  `nama_minat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_minat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `crm_minat`
--

INSERT INTO `crm_minat` (`id_minat`, `nama_minat`) VALUES
(1, 'jaringan'),
(2, 'Android'),
(3, 'Seni'),
(4, 'basket');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crm_polling_status`
--

CREATE TABLE IF NOT EXISTS `crm_polling_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(9) NOT NULL,
  `id_jadwal` int(1) NOT NULL,
  `waktu` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `crm_polling_status`
--

INSERT INTO `crm_polling_status` (`id`, `nim`, `id_jadwal`, `waktu`) VALUES
(1, '115410206', 83, '2015-01-21 14:09:16'),
(2, '105410201', 83, '2015-01-21 15:08:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE IF NOT EXISTS `dosen` (
  `dosen_ID` int(10) NOT NULL AUTO_INCREMENT,
  `id_level` int(1) NOT NULL DEFAULT '2',
  `username` varchar(11) COLLATE latin1_general_ci DEFAULT NULL,
  `password` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `NIDN` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TempatLahir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TanggalLahir` date DEFAULT NULL,
  `KTP` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Agama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` text COLLATE latin1_general_ci,
  `Email` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Telepon` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Handphone` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Kota` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Propinsi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Negara` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Identitas_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Homebase` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `jurusan_ID` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Gelar` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Jenjang_ID` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keilmuan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Kelamin_ID` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Jabatan_ID` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JabatanDikti_ID` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `InstitusiInduk` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBekerja` date NOT NULL,
  `StatusDosen_ID` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `StatusKerja_ID` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  `foto` varchar(100) COLLATE latin1_general_ci DEFAULT 'no foto.jpg',
  PRIMARY KEY (`dosen_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=26 ;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`dosen_ID`, `id_level`, `username`, `password`, `NIDN`, `nama_lengkap`, `TempatLahir`, `TanggalLahir`, `KTP`, `Agama`, `Alamat`, `Email`, `Telepon`, `Handphone`, `Keterangan`, `Kota`, `Propinsi`, `Negara`, `Identitas_ID`, `Homebase`, `jurusan_ID`, `Gelar`, `Jenjang_ID`, `Keilmuan`, `Kelamin_ID`, `Jabatan_ID`, `JabatanDikti_ID`, `InstitusiInduk`, `TglBekerja`, `StatusDosen_ID`, `StatusKerja_ID`, `Aktif`, `foto`) VALUES
(5, 2, '10111321063', 'a5b6ed0c630b875684f0cdef5f749c05', '10111321063', 'Pius Dian Anggoro', '', '1914-01-01', '', '0', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.Si.,M.Cs.', '0', 'Komputer', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(6, 2, '10111321064', '4f0092938fc1d71a2fe36d3e8c6fc11c', '10111321064', 'Adiyuda Prayitna', '', '1914-01-01', '', 'ISLAM', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.T., M.T.', '0', 'Matematika', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(7, 2, '10111321065', '4c51ef2b48ceb5ba151d6a9329d66926', '10111321065', 'Dara Kusumawati', '', '1914-01-01', '', 'ISLAM', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.E.,M.M.', '0', 'Komputer', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(4, 2, '10111321062', '974ac7be13c20a69618f644c672f2cbc', '10111321062', 'Sudarmanto', '', '1914-01-01', '', 'ISLAM', '', '[ email ]', '', '[ ponsel ]', '', '', '', '', '14032012', '', '101,102,103,201,202', 'Ir.,M.T', 'A', 'Komputer', '', 'A', '0', '', '0000-00-00', 'A', 'A', 'Y', 'no_foto.jpg'),
(8, 2, '10111321066', '1fce90bd3bc8e3c79c12627f49453265', '10111321066', 'Bambang P.D.P', '', '1914-01-01', '', 'ISLAM', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.Kom,S.E.,MMSI', '0', 'Komputer', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(10, 2, '10111321067', '3ea4a8e4d7a95ace878f907ab8b72d1b', '10111321067', 'Cuk Subiyantoro', '', '1914-01-01', '', 'ISLAM', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.Kom.,M.Kom', '0', 'Komputer', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(12, 2, '10111321068', '03d27092e5ca8ceffbc6cee9c9ab2ac3', '10111321068', 'Endang Wahyuningsih', '', '1914-01-01', '', '0', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.Kom.,M.Cs', '0', '', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(13, 2, '10111321071', '84d98f557a67cb0c44a97a1fc0bf44b9', '10111321071', 'Femi Dwi Astuti', '', '1914-01-01', '', 'ISLAM', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.Kom', '0', '', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(14, 2, '10111321072', 'c83d4d80918401bace4b1ecc059a2e2a', '10111321072', 'Enny Itje Sella', '', '1914-01-01', '', 'ISLAM', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.Si., M.Kom', '0', '', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(15, 2, '10111321073', '12ef026b3fcf1e5d78d31319af0cd236', '10111321073', 'Tlau Sakti Santosa', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'S.S., M.Hum', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(16, 2, '10111321074', '716d946b6a2e932856abdedcd9e85223', '10111321074', 'Wagito', '', '1914-01-01', '', '0', '', '', '', '', '', '', '', '', '14032012', '', '101,102,103,201,202', 'S.T M.T', '0', '', '', '0', '0', '', '0000-00-00', '0', '0', 'Y', 'no_foto.jpg'),
(17, 2, '10111321075', 'e67c07c70338100248d5b246a0292d27', '10111321075', 'Sri Redjeki', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'S.Si., M.Kom', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(18, 2, '10111321076', '4baabf8849ddef1f34aca0ba8e08b5fc', '10111321076', 'Febri Nova Lenti', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'S.Si., M.T', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(19, 2, '10111321077', '28e0b5eff27b9cd7dd92d05f6afba744', '10111321077', 'Henderi', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'S.Kom.,M.Kom', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(20, 2, '10111321078', 'ec44423eaa5a98afea938c757af65087', '10111321078', 'M.Guntara', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'Ir.,M.T', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(21, 2, '10111321079', '6e3d686fc64473277bae0d4412c31baf', '10111321079', 'Indra Yatini', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'S.Kom.,M.Kom', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(22, 2, '10111321080', '3f4ffe49476b9d7e959ae42193b9bb8e', '10111321080', 'F.Wiwiek Nurwiyati', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'Dra.,M.T', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(23, 2, '10111321081', '159e0c85d1debc9842fab3518314ef10', '10111321081', 'Y.Yohakim Marwanta', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'S.Kom.,M.Cs', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(24, 2, '10111321082', '80912e407bc5d13e4aa5df9e073ee354', '10111321082', 'Lucia Nugraheni Harnaningrum', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'S.Si., M.T', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg'),
(25, 2, '10111321083', '7a729bf3c3c88c2839983872df7fee9a', '10111321083', 'Erna Hudianti Pujiarini', '', '0000-00-00', NULL, '0', NULL, '', '', '', NULL, NULL, NULL, NULL, '14032012', NULL, '101,102,103,201,202', 'S.Si.,M.Cs', NULL, NULL, NULL, NULL, '', NULL, '0000-00-00', NULL, NULL, 'Y', 'no_foto.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dropdownawal`
--

CREATE TABLE IF NOT EXISTS `dropdownawal` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `menu_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=39 ;

--
-- Dumping data untuk tabel `dropdownawal`
--

INSERT INTO `dropdownawal` (`id`, `parent_id`, `judul`, `url`, `menu_order`, `aktif`) VALUES
(1, 0, 'Depan', '', 1, 'Y'),
(2, 0, 'Program', '', 2, 'Y'),
(3, 0, 'Pendaftaran', '', 3, 'Y'),
(4, 1, 'Halaman Depam', '', 1, 'Y'),
(5, 1, 'Profil', '', 2, 'Y'),
(7, 1, 'Visi dan Misi', '', 3, 'Y'),
(8, 0, 'Kegiatan', '', 4, 'Y'),
(9, 0, 'Download', '', 5, 'Y'),
(10, 0, 'Beasiswa', '', 6, 'Y'),
(11, 1, 'Fasilitas Kampus', '', 6, 'Y'),
(13, 2, 'Program Magiester Manajemen', '', 1, 'Y'),
(15, 2, 'Program S1 & D3 ', '', 3, 'Y'),
(17, 3, 'Jadwal Pendaftaran', '', 1, 'Y'),
(18, 3, 'Pendaftaran Online', '', 2, 'Y'),
(19, 9, 'Area Download', '', 1, 'Y'),
(20, 9, 'Download Formulir Pendaftaran', '', 2, 'Y'),
(21, 9, 'Download Jadwal Kuliah', '', 3, 'Y'),
(22, 10, 'Beasiswa', '', 1, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dropdownsystem`
--

CREATE TABLE IF NOT EXISTS `dropdownsystem` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL,
  `id_group` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `menu_order` int(10) NOT NULL,
  `keterangan` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=168 ;

--
-- Dumping data untuk tabel `dropdownsystem`
--

INSERT INTO `dropdownsystem` (`id`, `parent_id`, `id_group`, `judul`, `url`, `menu_order`, `keterangan`, `aktif`) VALUES
(1, 0, '1', 'Ba Akademik', '', 1, '', ''),
(2, 0, '2', 'Akademik', '', 2, '', 'Y'),
(4, 0, '4', 'Dosen', '', 4, '', 'Y'),
(144, 8, '', '01 Identitas', '?page=masteridentitas', 1, '', 'Y'),
(7, 0, '7', 'Mahasiswa', '', 7, '', 'Y'),
(8, 0, '8', 'Master', '', 8, '', 'Y'),
(10, 0, '10', 'System', '', 10, '', ''),
(141, 2, '', '07 KHS Mahasiswa', '?page=akademikkhs', 8, '', 'Y'),
(20, 2, '', '02 Kalender Akademik', '?page=akademiktahun', 2, 'Status Akademik', 'Y'),
(135, 2, '', '08 Registrasi Ulang Mahasiswa', '?page=registrasimahasiswa', 9, '', 'Y'),
(22, 2, '', '03 Penjadwalan Kuliah', '?page=jadkulAkd', 3, '', 'Y'),
(23, 2, '', '05 KRS Mahasiswa', '?page=akademikkrs', 6, 'KRS Mahasiswa utk Akademik', 'Y'),
(134, 0, '11', 'Exit', '', 11, '', 'Y'),
(31, 7, '', '04 Index Prestasi Kumulatif', '?page=mahasiswatranskrip', 4, '', 'Y'),
(33, 7, '', '02 Jadwal Ujian', '?page=mahasiswajadujian', 2, '', 'Y'),
(35, 7, '', '03 Kartu Hasil Studi (KHS)', '?page=mahasiswakhs', 3, '', 'Y'),
(36, 7, '', '01 Kartu Rencana Studi (KRS)', '?page=mahasiswakrs', 1, '', 'Y'),
(41, 10, '', '00 Admin Modul', '?page=modul', 0, '', 'Y'),
(110, 11, '', 'Exit', '?page=exit', 1, 'Modul Keluar', 'Y'),
(51, 10, '', '01 Admin User', '?page=AdminUser', 2, '', 'Y'),
(53, 10, '', '02 Admin Karyawan', '?page=AdminKaryawan', 4, '', 'Y'),
(157, 1, '', '01 Matakuliah', '?page=levelakademikmtk', 1, '', 'Y'),
(136, 4, '', '02 Nilai Mahasiswa', '?page=dosennilai', 2, '', 'Y'),
(137, 4, '', '01 Absen Kuliah', '?page=dosenajar', 1, '', 'Y'),
(143, 2, '', '09 Transkrip Nilai', '?page=akademiktranskrip', 10, '', 'Y'),
(145, 8, '', '02 Jurusan', '?page=masterjurusan', 2, '', 'Y'),
(146, 8, '', '03 Program', '?page=masterprogram', 3, '', 'Y'),
(147, 8, '', '04 Kampus', '?page=masterkampus', 4, '', 'Y'),
(148, 8, '', '05 Ruang', '?page=masterruang', 5, '', 'Y'),
(149, 8, '', '06 Dosen', '?page=masterdosen', 6, '', 'Y'),
(150, 2, '', '01 Matakuliah', '?page=akademikmatakuliah', 1, '', 'Y'),
(151, 0, '', '', '', 0, '', ''),
(153, 2, '', '06 Nilai Mahasiswa', '?page=akademiknilai', 7, '', 'Y'),
(155, 2, '', '04 Mahasiswa', '?page=akademikmahasiswa', 5, '', 'Y'),
(156, 0, '', '', '', 0, '', ''),
(158, 1, '', '02 Kalender Akademik', '?page=levelakademiktahunakd', 2, '', 'Y'),
(159, 1, '', '03 Penjadwalan Kuliah', '?page=levelakademikjadkul', 3, '', 'Y'),
(160, 1, '', '04 Mahasiswa', '?page=levelakademikmhs', 4, '', 'Y'),
(161, 1, '', '05 KRS Mahasiswa', '?page=levelakademikkrs', 5, '', 'Y'),
(162, 1, '', '06 Nilai Mahasiswa', '?page=levelakademiknilaimhs', 6, '', 'Y'),
(163, 1, '', '07 KHS Mahasiswa', '?page=levelakademikkhsmhs', 7, '', 'Y'),
(164, 1, '', '08 Registrasi Ulang Mahasiswa', '?page=levelakademikregmhs', 7, '', 'Y'),
(165, 1, '', '09 Transkrip Nilai', '?page=levelakademiktranskrip', 8, '', 'Y'),
(167, 0, '', '', '', 0, '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `error`
--

CREATE TABLE IF NOT EXISTS `error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabel` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `text` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `error`
--

INSERT INTO `error` (`id`, `tabel`, `text`) VALUES
(1, 'Group Modul', '1. Pengisian form berurutan sesuai dengan parent ID modul.\r\n2. Jika Terjadi Kesalahan yang tidak anda ketahui silakan hubungi administrator anda.\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `groupmodul`
--

CREATE TABLE IF NOT EXISTS `groupmodul` (
  `id_group` int(10) NOT NULL AUTO_INCREMENT,
  `relasi_modul` int(10) NOT NULL,
  `nama` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=58 ;

--
-- Dumping data untuk tabel `groupmodul`
--

INSERT INTO `groupmodul` (`id_group`, `relasi_modul`, `nama`) VALUES
(1, 1, 'Ba Akademik'),
(2, 2, 'Akademik'),
(4, 4, 'Dosen'),
(7, 7, 'Mahasiswa'),
(8, 8, 'Master'),
(10, 10, 'System'),
(57, 11, 'Exit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hakmodul`
--

CREATE TABLE IF NOT EXISTS `hakmodul` (
  `id_level` int(2) NOT NULL,
  `id` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `hakmodul`
--

INSERT INTO `hakmodul` (`id_level`, `id`) VALUES
(4, 110),
(4, 31),
(4, 35),
(4, 32),
(4, 36),
(4, 152),
(4, 33),
(3, 110),
(3, 157),
(3, 159),
(0, 110),
(2, 110),
(1, 110),
(1, 51),
(1, 166),
(1, 41),
(1, 53),
(0, 152),
(0, 31),
(0, 35),
(3, 162),
(2, 137),
(2, 136),
(1, 147),
(2, 134),
(2, 4),
(1, 144),
(4, 137),
(4, 136),
(4, 134),
(1, 145),
(1, 148),
(1, 146),
(1, 149),
(1, 150),
(1, 155),
(1, 135),
(1, 141),
(1, 22),
(1, 153),
(1, 20),
(1, 23),
(1, 143),
(1, 134),
(1, 10),
(1, 2),
(4, 7),
(4, 4),
(0, 32),
(0, 36),
(3, 165),
(0, 33),
(3, 160),
(0, 134),
(0, 7),
(1, 8),
(3, 163),
(3, 158),
(3, 161),
(3, 164),
(3, 134),
(3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hari`
--

CREATE TABLE IF NOT EXISTS `hari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hari` varchar(10) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `hari`
--

INSERT INTO `hari` (`id`, `hari`) VALUES
(1, 'Senin'),
(2, 'Selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat'),
(6, 'Sabtu'),
(7, 'Minggu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hidup`
--

CREATE TABLE IF NOT EXISTS `hidup` (
  `Hidup` char(3) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NA` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Hidup`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `hidup`
--

INSERT INTO `hidup` (`Hidup`, `Nama`, `NA`) VALUES
('1', 'Masih Hidup', 'N'),
('2', 'Sudah Meninggal', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `identitas`
--

CREATE TABLE IF NOT EXISTS `identitas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Identitas_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `KodeHukum` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Nama_Identitas` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `TglMulai` date NOT NULL DEFAULT '0000-00-00',
  `Alamat1` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Kota` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodePos` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Telepon` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Fax` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Website` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoAkta` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglAkta` date DEFAULT NULL,
  `NoSah` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglSah` date DEFAULT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'Y',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=24 ;

--
-- Dumping data untuk tabel `identitas`
--

INSERT INTO `identitas` (`ID`, `Identitas_ID`, `KodeHukum`, `Nama_Identitas`, `TglMulai`, `Alamat1`, `Kota`, `KodePos`, `Telepon`, `Fax`, `Email`, `Website`, `NoAkta`, `TglAkta`, `NoSah`, `TglSah`, `Aktif`) VALUES
(22, '14032012', 'xx.xxx.xxx/xxxx', 'STMIK AKAKOM', '1983-03-01', 'Jl.Raya Janti 143 karang jambe', 'Yogyakarta', '55198', '0274-486664', '0274486438', 'info@akakom.ac.id', 'http://www.akakom.ac.id', '', '1992-06-08', '262/DIKTI/Kep/1992', '1992-06-08', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
  `Jabatan_ID` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Def` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `NA` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Jabatan_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`Jabatan_ID`, `Nama`, `Def`, `NA`) VALUES
('A', 'Tenaga Pengajar', 'N', 'N'),
('B', 'Asisten Ahli', 'N', 'N'),
('C', 'Lektor', 'N', 'N'),
('D', 'Lektor Kepala', 'N', 'N'),
('E', 'Guru Besar', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatandikti`
--

CREATE TABLE IF NOT EXISTS `jabatandikti` (
  `JabatanDikti_ID` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Def` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `NA` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`JabatanDikti_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `jabatandikti`
--

INSERT INTO `jabatandikti` (`JabatanDikti_ID`, `Nama`, `Def`, `NA`) VALUES
('01', 'AAM', 'N', 'N'),
('02', 'AA', 'N', 'N'),
('03', 'LMu', 'N', 'N'),
('04', 'LMa', 'N', 'N'),
('05', 'L', 'N', 'N'),
('06', 'LKM', 'N', 'N'),
('07', 'LK', 'N', 'N'),
('08', 'GBM', 'N', 'N'),
('09', 'GB', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE IF NOT EXISTS `jadwal` (
  `Jadwal_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Tahun_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Identitas_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Program_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Kode_Mtk` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Kode_Jurusan` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Ruang_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Kelas` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Dosen_ID` int(11) NOT NULL,
  `Hari` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Jam_Mulai` time NOT NULL DEFAULT '00:00:00',
  `Jam_Selesai` time NOT NULL DEFAULT '00:00:00',
  `UTSTgl` date NOT NULL,
  `UTSHari` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `UTSMulai` time NOT NULL,
  `UTSSelesai` time NOT NULL,
  `UTSRuang` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `UASTgl` date NOT NULL,
  `UASHari` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `UASMulai` time NOT NULL,
  `UASSelesai` time NOT NULL,
  `UASRuang` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `JumlahMhsw` int(11) NOT NULL,
  `Kapasitas` int(11) NOT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`Jadwal_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=92 ;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`Jadwal_ID`, `Tahun_ID`, `Identitas_ID`, `Program_ID`, `Kode_Mtk`, `Kode_Jurusan`, `Ruang_ID`, `Kelas`, `Dosen_ID`, `Hari`, `Jam_Mulai`, `Jam_Selesai`, `UTSTgl`, `UTSHari`, `UTSMulai`, `UTSSelesai`, `UTSRuang`, `UASTgl`, `UASHari`, `UASMulai`, `UASSelesai`, `UASRuang`, `JumlahMhsw`, `Kapasitas`, `Aktif`) VALUES
(62, '17', '14032012', '3', 'TI609UT', '202', '48', 'TI-1', 20, 'Jumat', '08:00:00', '09:40:00', '2014-04-21', 'Senin', '13:00:00', '14:50:00', '48', '2014-07-03', 'Rabu', '13:00:00', '14:50:00', '48', 0, 0, 'Y'),
(61, '17', '14032012', '3', 'TI201UP', '202', '158', 'TI-1', 14, 'Senin', '15:00:00', '16:45:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(60, '17', '14032012', '3', 'TI201UP', '202', '158', 'TI-2', 23, 'Kamis', '13:00:00', '14:45:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(59, '17', '14032012', '3', 'TI201UG', '202', '63', 'TI-2', 13, 'Rabu', '17:00:00', '19:00:00', '2014-04-21', 'Senin', '10:00:00', '11:55:00', '33', '2014-07-01', 'Selasa', '10:00:00', '838:59:59', '33', 0, 0, 'Y'),
(58, '17', '14032012', '3', 'TI201UG', '202', '38', 'TI-1', 23, 'Senin', '07:30:00', '10:00:00', '2014-04-21', 'Senin', '10:00:00', '11:55:00', '193', '2014-07-01', 'Selasa', '10:00:00', '11:55:00', '193', 0, 0, 'Y'),
(57, '17', '14032012', '3', 'TI402UT', '202', '93', 'TI-2', 24, 'Jumat', '10:00:00', '11:45:00', '2014-04-30', 'Rabu', '08:00:00', '09:55:00', '138', '2014-07-10', 'Kamis', '08:00:00', '09:55:00', '138', 0, 0, 'Y'),
(56, '17', '14032012', '3', 'TI402UT', '202', '128', 'TI-1', 4, 'Kamis', '07:30:00', '10:00:00', '2014-04-30', 'Rabu', '08:00:00', '09:55:00', '128', '2014-07-10', 'Kamis', '08:00:00', '09:55:00', '128', 0, 0, 'Y'),
(55, '17', '14032012', '3', 'TI401UP', '202', '153', 'TI-2', 18, 'Jumat', '10:00:00', '11:45:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(54, '17', '14032012', '3', 'TI401UP', '202', '178', 'TI-1', 12, 'Selasa', '13:00:00', '14:45:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(53, '17', '14032012', '3', 'TI602UT', '202', '78', 'TI-1', 17, 'Rabu', '15:00:00', '17:30:00', '2014-04-21', 'Senin', '15:00:00', '17:50:00', '78', '2014-07-01', 'Selasa', '15:00:00', '17:50:00', '78', 0, 0, 'Y'),
(51, '17', '14032012', '3', 'TI501UT', '202', '58', 'TI-1', 15, 'Selasa', '08:00:00', '09:40:00', '2014-04-25', 'Jumat', '08:00:00', '09:50:00', '193', '2014-07-03', 'Kamis', '08:00:00', '09:50:00', '193', 0, 0, 'Y'),
(49, '17', '14032012', '3', 'TI401UG', '202', '63', 'TI-5', 17, 'Senin', '07:30:00', '10:00:00', '2014-04-24', 'Kamis', '10:15:00', '11:50:00', '93', '2014-07-04', 'Rabu', '10:15:00', '11:50:00', '93', 0, 0, 'Y'),
(50, '17', '14032012', '3', 'TI401UG', '202', '138', 'TI-3', 10, 'Rabu', '15:00:00', '17:30:00', '2014-04-24', 'Kamis', '10:15:00', '11:50:00', '83', '2014-07-04', 'Rabu', '10:15:00', '11:50:00', '83', 0, 0, 'Y'),
(63, '17', '14032012', '3', 'TI702UK', '202', '0', 'TI-1', 4, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(64, '17', '14032012', '3', 'TI702UK', '202', '0', 'TI-2', 10, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(65, '17', '14032012', '3', 'TI701PK', '202', '0', '', 0, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(66, '17', '14032012', '3', 'TI801UK', '202', '0', 'TI-1', 4, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(67, '17', '14032012', '3', 'TI801UK', '202', '0', 'TI-2', 13, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(68, '5', '14032012', '3', 'TI101UT', '202', '48', 'TI-1', 20, 'Senin', '07:30:00', '10:00:00', '2014-10-27', 'Senin', '08:00:00', '10:30:00', '193', '2015-01-05', 'Senin', '08:00:00', '10:30:00', '193', 0, 0, 'Y'),
(69, '5', '14032012', '3', 'TI101UT', '202', '133', 'TI-2', 13, 'Kamis', '15:00:00', '17:30:00', '2014-10-27', 'Senin', '08:00:00', '10:30:00', '33', '2015-01-05', 'Senin', '08:00:00', '10:30:00', '33', 0, 0, 'Y'),
(70, '5', '14032012', '3', 'TI103LT', '202', '58', 'TI-1', 15, 'Selasa', '08:00:00', '09:40:00', '2014-10-30', 'Kamis', '10:15:00', '12:30:00', '58', '2015-01-08', 'Kamis', '10:15:00', '12:30:00', '58', 0, 0, 'Y'),
(71, '5', '14032012', '3', 'TI103LT', '202', '123', 'TI-2', 15, 'Jumat', '19:00:00', '20:40:00', '2014-10-30', 'Kamis', '10:15:00', '12:30:00', '63', '2015-01-08', 'Kamis', '10:15:00', '12:30:00', '63', 0, 0, 'Y'),
(72, '5', '14032012', '3', 'TI101UP', '202', '153', 'TI-1', 20, 'Kamis', '13:00:00', '15:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(73, '5', '14032012', '3', 'TI101UP', '202', '153', 'TI-2', 13, 'Rabu', '10:00:00', '12:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(74, '5', '14032012', '3', 'TI301UT', '202', '48', 'TI-1', 12, 'Selasa', '08:00:00', '09:40:00', '2014-10-27', 'Senin', '13:00:00', '14:40:00', '48', '2015-01-05', 'Senin', '13:00:00', '14:40:00', '48', 0, 0, 'Y'),
(75, '5', '14032012', '3', 'TI301UT', '202', '98', 'TI-2', 10, 'Rabu', '15:00:00', '16:40:00', '2014-10-27', 'Senin', '13:00:00', '14:40:00', '63', '2015-01-05', 'Senin', '13:00:00', '14:40:00', '63', 0, 0, 'Y'),
(76, '5', '14032012', '3', 'TI301UP', '202', '168', 'TI-1', 12, 'Selasa', '10:00:00', '12:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(77, '5', '14032012', '3', 'TI301UP', '202', '153', 'TI-2', 10, 'Jumat', '15:00:00', '17:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(78, '5', '14032012', '3', 'TI302UT', '202', '123', 'TI-1', 24, 'Jumat', '10:00:00', '12:30:00', '2014-10-31', 'Jumat', '10:00:00', '12:15:00', '123', '2015-01-09', 'Jumat', '10:00:00', '12:15:00', '123', 0, 0, 'Y'),
(79, '5', '14032012', '3', 'TI302UT', '202', '93', 'TI-2', 21, 'Senin', '15:00:00', '17:30:00', '2014-10-31', 'Jumat', '10:00:00', '12:15:00', '133', '2015-01-09', 'Jumat', '10:00:00', '12:15:00', '133', 0, 0, 'Y'),
(80, '5', '14032012', '3', 'TI508UT', '202', '153', 'TI-1', 23, 'Senin', '13:00:00', '15:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '2015-01-09', 'Jumat', '13:00:00', '15:15:00', '158', 0, 0, 'Y'),
(81, '5', '14032012', '3', 'TI708UG', '202', '138', 'TI-1', 16, 'Kamis', '15:00:00', '14:40:00', '2014-10-28', 'Selasa', '15:00:00', '16:40:00', '138', '2015-01-06', 'Selasa', '15:00:00', '16:40:00', '138', 0, 0, 'Y'),
(82, '5', '14032012', '3', 'TI502UT', '202', '128', 'TI-1', 25, 'Senin', '15:00:00', '17:30:00', '2014-10-27', 'Senin', '10:15:00', '12:15:00', '193', '2015-01-05', 'Senin', '10:15:00', '12:15:00', '193', 0, 0, 'Y'),
(83, '5', '14032012', '3', 'TI502UT', '202', '103', 'TI-2', 4, 'Selasa', '10:00:00', '12:30:00', '2014-10-27', 'Senin', '10:15:00', '12:15:00', '33', '2015-01-05', 'Senin', '10:15:00', '12:15:00', '33', 0, 0, 'Y'),
(84, '5', '14032012', '3', 'TI501UP', '202', '163', 'TI-1', 5, 'Rabu', '15:00:00', '17:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(85, '5', '14032012', '3', 'TI501UP', '202', '168', 'TI-1', 13, 'Kamis', '08:00:00', '10:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(86, '5', '14032012', '3', 'TI701PK', '202', '0', 'TI-1', 0, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(87, '5', '14032012', '3', 'TI702UK', '202', '0', 'TI-1', 7, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(88, '5', '14032012', '3', 'TI702UK', '202', '0', '', 18, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(89, '5', '14032012', '3', 'TI801UK', '202', '0', '', 21, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(90, '5', '14032012', '3', 'TI801UK', '202', '0', '', 4, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y'),
(91, '5', '14032012', '3', 'TI801UK', '202', '0', '', 17, '0', '00:00:00', '00:00:00', '0000-00-00', '', '00:00:00', '00:00:00', '', '0000-00-00', '', '00:00:00', '00:00:00', '', 0, 0, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jeniskurikulum`
--

CREATE TABLE IF NOT EXISTS `jeniskurikulum` (
  `JenisKurikulum_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Kode` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Singkatan` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Jurusan_ID` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`JenisKurikulum_ID`),
  KEY `Jurusan_ID` (`Jurusan_ID`),
  KEY `Identitas_ID` (`Kode`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `jeniskurikulum`
--

INSERT INTO `jeniskurikulum` (`JenisKurikulum_ID`, `Kode`, `Singkatan`, `Nama`, `Jurusan_ID`, `Aktif`) VALUES
(1, 'A', NULL, 'Inti', '', 'N'),
(2, 'B', NULL, 'Institusi', '', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenismk`
--

CREATE TABLE IF NOT EXISTS `jenismk` (
  `JenisMK_ID` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `Identitas_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Urutan` int(11) NOT NULL DEFAULT '0',
  `Singkatan` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Jurusan_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`JenisMK_ID`),
  KEY `ProdiID` (`Jurusan_ID`),
  KEY `KodeID` (`Identitas_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `jenismk`
--

INSERT INTO `jenismk` (`JenisMK_ID`, `Identitas_ID`, `Urutan`, `Singkatan`, `Nama`, `Jurusan_ID`, `Aktif`) VALUES
('W', '', 0, NULL, 'WAJIB', '', 'Y'),
('P', '', 0, NULL, 'PILIHAN', '', 'Y'),
('WM', '', 0, NULL, 'WAJIB KEMINATAN', '', 'Y'),
('D', '', 0, NULL, 'PILIHAN PERMINTAAN', '', 'Y'),
('S', '', 0, NULL, 'TA/SKRIPSI/THESIS/DISERTASI', '', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenissekolah`
--

CREATE TABLE IF NOT EXISTS `jenissekolah` (
  `JenisSekolah_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `SatuGroup` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'N',
  `NA` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'N',
  `TemplateSuratPMB` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`JenisSekolah_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `jenissekolah`
--

INSERT INTO `jenissekolah` (`JenisSekolah_ID`, `Nama`, `SatuGroup`, `NA`, `TemplateSuratPMB`) VALUES
('PENABUR', 'Yayasan Penabur', 'Y', 'N', NULL),
('KRISTEN', 'Kristen/Katolik Non Penabur', 'N', 'N', ''),
('UMUM', 'Umum', 'N', 'N', NULL),
('NEGERI', 'Negeri', 'N', 'N', ''),
('LN', 'Luar Negeri', 'N', 'N', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_ujian`
--

CREATE TABLE IF NOT EXISTS `jenis_ujian` (
  `jenisjadwal` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`jenisjadwal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `jenis_ujian`
--

INSERT INTO `jenis_ujian` (`jenisjadwal`, `nama`) VALUES
('UTS', 'Ujian Tengah Semester'),
('UAS', 'Ujian Akhir Semester');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenjang`
--

CREATE TABLE IF NOT EXISTS `jenjang` (
  `Jenjang_ID` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Def` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `NA` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Jenjang_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `jenjang`
--

INSERT INTO `jenjang` (`Jenjang_ID`, `Nama`, `Keterangan`, `Def`, `NA`) VALUES
('B', 'D3', NULL, 'N', 'N'),
('A', 'S1', '', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE IF NOT EXISTS `jurusan` (
  `jurusan_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Identitas_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `kode_jurusan` varchar(11) COLLATE latin1_general_ci NOT NULL,
  `nama_jurusan` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `jenjang` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `Akreditasi` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSKDikti` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TglSKDikti` date DEFAULT NULL,
  `NoSKBAN` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TglSKBAN` date DEFAULT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`jurusan_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`jurusan_ID`, `Identitas_ID`, `kode_jurusan`, `nama_jurusan`, `jenjang`, `Akreditasi`, `NoSKDikti`, `TglSKDikti`, `NoSKBAN`, `TglSKBAN`, `Aktif`) VALUES
(10, '14032012', '201', 'Sistem Informasi', 'S1', 'B', '300/DIKTI/Kep/1992', '1914-01-01', '028/BAN-PT/Ak-XIV/S1/2011', '1914-01-01', 'Y'),
(7, '14032012', '101', 'Komputerisasi Akuntansi', 'D3', 'B', '39/DIKTI/Kep/1999', '1999-01-01', '007/BAN-PT/Ak-XII/Dip-III/V/2012', '2012-05-16', 'Y'),
(8, '14032012', '102', 'Manajemen Informatika', 'D3', 'A', '', '1914-01-01', '', '1914-01-01', 'Y'),
(9, '14032012', '103', 'Teknik Komputer', 'D3', 'A', '', '1914-01-01', '', '1914-01-01', 'Y'),
(11, '14032012', '202', 'Teknik Informatika', 'S1', 'B', '300/DIKTI/Kep/1992', '0000-00-00', '003/BAN-PT/Ak-XIV/S1/V/2011', '0000-00-00', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusansekolah`
--

CREATE TABLE IF NOT EXISTS `jurusansekolah` (
  `JurusanSekolah_ID` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaJurusan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `NA` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'N',
  PRIMARY KEY (`JurusanSekolah_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `jurusansekolah`
--

INSERT INTO `jurusansekolah` (`JurusanSekolah_ID`, `Nama`, `NamaJurusan`, `NA`) VALUES
('011', 'SMU', 'IPA', 'N'),
('012', 'SMU', 'IPS', 'N'),
('013', 'SMU', 'A4/BAHASA', 'N'),
('021', 'STM PEMBANGUNAN', 'BANGUNAN GEDUNG', 'N'),
('022', 'STM PEMBANGUNAN', 'BANGUNAN AIR', 'N'),
('023', 'STM PEMBANGUNAN', 'MESIN PRODUKSI', 'N'),
('025', 'STM PEMBANGUNAN', 'LISTRIK INDUSTRI', 'N'),
('024', 'STM PEMBANGUNAN', 'OTOMOTIF', 'N'),
('027', 'STM PEMBANGUNAN', 'ELEKTRO KOMUNIKASI', 'N'),
('031', 'SMEA', 'TATA BUKU', 'N'),
('032', 'SMEA', 'TATA NIAGA', 'N'),
('033', 'SMEA', 'TATA USAHA', 'N'),
('101', 'STM', 'ELEKTRONIKA', 'N'),
('102', 'STM', 'LISTRIK', 'N'),
('103', 'STM', 'MESIN PRODUKSI', 'N'),
('104', 'STM', 'BANGUNAN', 'N'),
('105', 'STM', 'OTOMOTIF', 'N'),
('121', 'SMTP', 'BANGUNAN KAPAL', 'N'),
('122', 'SMTP', 'MESIN KAPAL', 'N'),
('131', 'SMTP', 'AVIONIKA', 'N'),
('132', 'SMTP', 'LISTRIK & INSTRUMEN', 'N'),
('133', 'SMTP', 'MOTOR & RANGKA', 'N'),
('350', 'SMEA PEMBANGUNAN', 'EKONOMI', 'N'),
('014', 'SMU', 'A1', 'N'),
('015', 'SMU', 'A2', 'N'),
('016', 'SMU', 'A3', 'N'),
('161', 'SPG', 'SD', 'N'),
('162', 'SPG', 'TK', 'N'),
('999', 'SMA LUAR NEGERI', '', 'N'),
('041', 'SMF', 'FARMASI', 'N'),
('042', 'SA KES', 'ANALISI KESEHATAN', 'N'),
('034', 'SMEA', 'SEKRETARIS', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kampus`
--

CREATE TABLE IF NOT EXISTS `kampus` (
  `Kampus_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Kota` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Identitas_ID` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `Telepon` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Fax` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'N',
  PRIMARY KEY (`Kampus_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `kampus`
--

INSERT INTO `kampus` (`Kampus_ID`, `Nama`, `Alamat`, `Kota`, `Identitas_ID`, `Telepon`, `Fax`, `Aktif`) VALUES
('K1', 'KAMPUS 1', '', '', '14032012', '', '', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(2) NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `Identitas_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `kode_jurusan` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `keterangan` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `telepon` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `agama` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `alamat` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  `username` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id`, `id_level`, `nama_lengkap`, `Identitas_ID`, `kode_jurusan`, `keterangan`, `email`, `telepon`, `agama`, `alamat`, `aktif`, `username`, `password`) VALUES
(11, 3, 'EDO IKFIANDA', '14032012', '263', '-', 'edoikfianda@yahoo.co.id', '-', '', '', 'Y', 'akademiksk', 'c724a2603f3047be02da3107a7dd3ce9'),
(10, 3, 'SUPRIYATI', '14032012', '261', '-', 'yati@email.com', '-', '', '', 'Y', 'akademiksi', '367de297f24524dc97c52401405df28a');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompokmtk`
--

CREATE TABLE IF NOT EXISTS `kelompokmtk` (
  `KelompokMtk_ID` varchar(4) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `kelompokmtk`
--

INSERT INTO `kelompokmtk` (`KelompokMtk_ID`, `Nama`, `Aktif`) VALUES
('A', 'UTAMA', 'Y'),
('B', 'PENDUKUNG', 'Y'),
('C', 'UMUM', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `krs`
--

CREATE TABLE IF NOT EXISTS `krs` (
  `KRS_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NIM` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Tahun_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Jadwal_ID` bigint(20) NOT NULL DEFAULT '0',
  `SKS` int(11) NOT NULL DEFAULT '0',
  `Tugas1` int(11) NOT NULL DEFAULT '0',
  `Tugas2` int(11) NOT NULL DEFAULT '0',
  `Tugas3` int(11) NOT NULL DEFAULT '0',
  `Tugas4` int(11) NOT NULL DEFAULT '0',
  `Tugas5` int(11) NOT NULL DEFAULT '0',
  `Presensi` int(11) NOT NULL DEFAULT '0',
  `UTS` int(11) NOT NULL DEFAULT '0',
  `UAS` int(11) NOT NULL DEFAULT '0',
  `GradeNilai` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT '-',
  `BobotNilai` decimal(4,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`KRS_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=152 ;

--
-- Dumping data untuk tabel `krs`
--

INSERT INTO `krs` (`KRS_ID`, `NIM`, `Tahun_ID`, `Jadwal_ID`, `SKS`, `Tugas1`, `Tugas2`, `Tugas3`, `Tugas4`, `Tugas5`, `Presensi`, `UTS`, `UAS`, `GradeNilai`, `BobotNilai`) VALUES
(1, '115410206', '2014-1', 83, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(2, '105410201', '2014-1', 83, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(3, '115410224', '2014-1', 68, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(4, '115410224', '2014-1', 70, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(5, '115410224', '2014-1', 73, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(6, '115410224', '2014-1', 75, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 1.00),
(7, '115410224', '2014-1', 76, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 1.00),
(8, '115410224', '2014-1', 79, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(9, '115410224', '2014-1', 80, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 1.00),
(10, '115410224', '2014-1', 81, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 1.00),
(11, '115410224', '2014-1', 83, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(12, '115410224', '2014-1', 84, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(13, '115410224', '2014-1', 86, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(14, '115410224', '2014-1', 88, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(15, '115410224', '2014-1', 89, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(16, '105410201', '2014-1', 68, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(17, '105410201', '2014-1', 70, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(18, '105410201', '2014-1', 72, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(19, '105410201', '2014-1', 75, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(20, '105410201', '2014-1', 77, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(21, '105410201', '2014-1', 79, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(22, '105410201', '2014-1', 80, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(23, '105410201', '2014-1', 81, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(24, '105410201', '2014-1', 84, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(25, '105410201', '2014-1', 86, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(26, '105410201', '2014-1', 87, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(27, '105410201', '2014-1', 91, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(28, '105410254', '2014-1', 69, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(29, '105410254', '2014-1', 71, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(30, '105410254', '2014-1', 73, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 1.00),
(31, '105410254', '2014-1', 75, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(32, '105410254', '2014-1', 77, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(33, '105410254', '2014-1', 79, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(34, '105410254', '2014-1', 80, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(35, '105410254', '2014-1', 81, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(36, '105410254', '2014-1', 83, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(37, '105410254', '2014-1', 84, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(38, '105410254', '2014-1', 86, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(39, '105410254', '2014-1', 87, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(40, '105410254', '2014-1', 90, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(41, '115410206', '2014-1', 69, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(42, '115410206', '2014-1', 70, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(43, '115410206', '2014-1', 72, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(44, '115410206', '2014-1', 75, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(45, '115410206', '2014-1', 77, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(46, '115410206', '2014-1', 79, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(47, '115410206', '2014-1', 80, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(48, '115410206', '2014-1', 81, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(49, '115410206', '2014-1', 84, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(50, '115410206', '2014-1', 86, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(51, '115410206', '2014-1', 88, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(52, '115410206', '2014-1', 90, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(53, '125410101', '2014-1', 69, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(54, '125410101', '2014-1', 71, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(55, '125410101', '2014-1', 72, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(56, '125410101', '2014-1', 74, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(57, '125410101', '2014-1', 76, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(58, '125410101', '2014-1', 78, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 1.00),
(59, '125410101', '2014-1', 80, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(60, '125410101', '2014-1', 81, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(61, '125410101', '2014-1', 82, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(62, '125410101', '2014-1', 85, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(63, '125410101', '2014-1', 86, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(64, '125410101', '2014-1', 87, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(65, '125410101', '2014-1', 91, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(66, '135410128', '2014-1', 68, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(67, '135410128', '2014-1', 70, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(68, '135410128', '2014-1', 72, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(69, '135410128', '2014-1', 75, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(70, '135410128', '2014-1', 76, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(71, '135410128', '2014-1', 78, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(72, '135410128', '2014-1', 80, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(73, '135410128', '2014-1', 81, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(74, '135410128', '2014-1', 82, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(75, '135410128', '2014-1', 85, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(76, '135410128', '2014-1', 86, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(77, '135410128', '2014-1', 88, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(78, '135410128', '2014-1', 89, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(79, '135410206', '2014-1', 69, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(80, '135410206', '2014-1', 71, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(81, '135410206', '2014-1', 72, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(82, '135410206', '2014-1', 75, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(83, '135410206', '2014-1', 77, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(84, '135410206', '2014-1', 78, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(85, '135410206', '2014-1', 80, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(86, '135410206', '2014-1', 81, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(87, '135410206', '2014-1', 82, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(88, '135410206', '2014-1', 85, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(89, '135410206', '2014-1', 86, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(90, '135410206', '2014-1', 87, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(91, '135410206', '2014-1', 90, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(92, '105410201', '2013-2', 49, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(93, '105410201', '2013-2', 51, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(94, '105410201', '2013-2', 53, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(95, '105410201', '2013-2', 54, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(96, '105410201', '2013-2', 56, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(97, '105410201', '2013-2', 58, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(98, '105410201', '2013-2', 60, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(99, '105410201', '2013-2', 62, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 1.00),
(100, '105410201', '2013-2', 63, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(101, '105410201', '2013-2', 65, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(102, '105410201', '2013-2', 66, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(103, '105410254', '2013-2', 49, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(104, '105410254', '2013-2', 51, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(105, '105410254', '2013-2', 53, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(106, '105410254', '2013-2', 54, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(107, '105410254', '2013-2', 56, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(108, '105410254', '2013-2', 58, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(109, '105410254', '2013-2', 60, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(110, '105410254', '2013-2', 62, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(111, '105410254', '2013-2', 63, 6, 0, 0, 0, 0, 0, 0, 0, 0, '-', 0.00),
(112, '115410206', '2013-2', 50, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(113, '115410206', '2013-2', 51, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(114, '115410206', '2013-2', 53, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(115, '115410206', '2013-2', 55, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(116, '115410206', '2013-2', 57, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(117, '115410206', '2013-2', 59, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(118, '115410206', '2013-2', 61, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(119, '115410206', '2013-2', 62, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(120, '115410224', '2013-2', 50, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(121, '115410224', '2013-2', 51, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(122, '115410224', '2013-2', 53, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(123, '115410224', '2013-2', 54, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(124, '115410224', '2013-2', 57, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(125, '115410224', '2013-2', 58, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'D', 1.00),
(126, '115410224', '2013-2', 61, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(127, '115410224', '2013-2', 62, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(128, '125410101', '2013-2', 50, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(129, '125410101', '2013-2', 51, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(130, '125410101', '2013-2', 53, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(131, '125410101', '2013-2', 55, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(132, '125410101', '2013-2', 56, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(133, '125410101', '2013-2', 59, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(134, '125410101', '2013-2', 61, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(135, '125410101', '2013-2', 62, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(136, '135410128', '2013-2', 49, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(137, '135410128', '2013-2', 51, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(138, '135410128', '2013-2', 54, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(139, '135410128', '2013-2', 56, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(140, '135410128', '2013-2', 58, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(141, '135410128', '2013-2', 60, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(142, '135410128', '2013-2', 62, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(143, '135410206', '2013-2', 50, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(144, '135410206', '2013-2', 51, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(145, '135410206', '2013-2', 54, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(146, '135410206', '2013-2', 57, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(147, '135410206', '2013-2', 58, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'C', 2.00),
(148, '135410206', '2013-2', 61, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'A', 4.00),
(149, '135410206', '2013-2', 62, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(150, '135410128', '2013-2', 53, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00),
(151, '135410206', '2013-2', 53, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'B', 3.00);

-- --------------------------------------------------------

--
-- Stand-in structure for view `krs1`
--
CREATE TABLE IF NOT EXISTS `krs1` (
`id` bigint(20)
,`idjdwl` bigint(20)
,`identitas_ID` varchar(10)
,`idprog` varchar(20)
,`nama_program` varchar(100)
,`kode_jurusan` varchar(20)
,`nama_jurusan` varchar(100)
,`tahun` varchar(10)
,`idtahun` int(11)
,`TglCetakKHS` date
,`Aktif` enum('Y','N')
,`NIM` varchar(50)
,`nama_lengkap` varchar(100)
,`Ruang_ID` varchar(10)
,`kelas` varchar(20)
,`kode_mtk` varchar(10)
,`nama_matakuliah` varchar(100)
,`semester` varchar(2)
,`hari` varchar(10)
,`jam_mulai` time
,`jam_selesai` time
,`sks` varchar(2)
,`tugas1` int(11)
,`tugas2` int(11)
,`tugas3` int(11)
,`tugas4` int(11)
,`nilai_mid` int(11)
,`nilai_uas` int(11)
,`UTSTgl` date
,`UTSMulai` time
,`UTSSelesai` time
,`UTSRuang` varchar(10)
,`UASTgl` date
,`UASMulai` time
,`UASSelesai` time
,`UASRuang` varchar(10)
,`nilai_tgs` decimal(16,2)
,`nilai_akhir` decimal(17,2)
,`GradeNilai` varchar(10)
,`bobot` varchar(2)
,`dosen` varchar(100)
,`gelar` varchar(50)
);
-- --------------------------------------------------------

--
-- Struktur dari tabel `kurikulum`
--

CREATE TABLE IF NOT EXISTS `kurikulum` (
  `Kurikulum_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Kode` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Identitas_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Jurusan_ID` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Sesi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JmlSesi` int(11) NOT NULL DEFAULT '2',
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Kurikulum_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=29 ;

--
-- Dumping data untuk tabel `kurikulum`
--

INSERT INTO `kurikulum` (`Kurikulum_ID`, `Kode`, `Nama`, `Identitas_ID`, `Jurusan_ID`, `Sesi`, `JmlSesi`, `Aktif`) VALUES
(28, '20141', 'Kurikulum 2014/2015', '14032012', '103', 'Semester', 2, 'Y'),
(24, '20141', 'Kurikulum 2014/2015', '14032012', '202', 'Semester', 2, 'Y'),
(27, '20141', 'Kurikulum 2014/2015', '14032012', '102', 'Semester', 2, 'Y'),
(26, '20141', 'Kurikulum 2014/2015', '14032012', '101', 'Semester', 2, 'Y'),
(25, '20141', 'Kurikulum 2014/2015', '14032012', '201', 'Semester', 2, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(10) NOT NULL,
  `level` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `level`) VALUES
(1, 'Administrator'),
(2, 'Dosen'),
(3, 'Akademik'),
(4, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `NIM` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `level_ID` int(11) NOT NULL DEFAULT '4',
  `username` varchar(9) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Angkatan` varchar(8) COLLATE latin1_general_ci DEFAULT NULL,
  `Kurikulum_ID` int(11) NOT NULL,
  `identitas_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Foto` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT 'no foto.jpg',
  `StatusAwal_ID` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `StatusMhsw_ID` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `IDProg` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `kode_jurusan` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `PenasehatAkademik` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Kelamin` char(3) COLLATE latin1_general_ci DEFAULT NULL,
  `WargaNegara` char(3) COLLATE latin1_general_ci DEFAULT NULL,
  `Kebangsaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TempatLahir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TanggalLahir` date DEFAULT NULL,
  `Agama` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `StatusSipil` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Kota` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `RT` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `RW` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodePos` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Propinsi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Negara` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Telepon` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Handphone` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatAsal` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KotaAsal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `RTAsal` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `RWAsal` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodePosAsal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `PropinsiAsal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NegaraAsal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaAyah` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `AgamaAyah` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `PendidikanAyah` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `PekerjaanAyah` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `HidupAyah` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaIbu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `AgamaIbu` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `PendidikanIbu` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `PekerjaanIbu` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `HidupIbu` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatOrtu` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KotaOrtu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodePosOrtu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `PropinsiOrtu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NegaraOrtu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TeleponOrtu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `HandphoneOrtu` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `EmailOrtu` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `AsalSekolah` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `AsalSekolah1` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisSekolah_ID` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KotaSekolah` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JurusanSekolah` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NilaiSekolah` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `TahunLulus` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'N',
  `LulusUjian` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `NilaiUjian` float unsigned NOT NULL DEFAULT '0',
  `GradeNilai` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `TanggalLulus` date NOT NULL DEFAULT '0000-00-00' COMMENT 'Lulus dari perguruan tinggi',
  `IPK` decimal(4,2) NOT NULL DEFAULT '0.00',
  `TotalSKS` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`NIM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`NIM`, `level_ID`, `username`, `password`, `Angkatan`, `Kurikulum_ID`, `identitas_ID`, `Nama`, `Foto`, `StatusAwal_ID`, `StatusMhsw_ID`, `IDProg`, `kode_jurusan`, `PenasehatAkademik`, `Kelamin`, `WargaNegara`, `Kebangsaan`, `TempatLahir`, `TanggalLahir`, `Agama`, `StatusSipil`, `Alamat`, `Kota`, `RT`, `RW`, `KodePos`, `Propinsi`, `Negara`, `Telepon`, `Handphone`, `Email`, `AlamatAsal`, `KotaAsal`, `RTAsal`, `RWAsal`, `KodePosAsal`, `PropinsiAsal`, `NegaraAsal`, `NamaAyah`, `AgamaAyah`, `PendidikanAyah`, `PekerjaanAyah`, `HidupAyah`, `NamaIbu`, `AgamaIbu`, `PendidikanIbu`, `PekerjaanIbu`, `HidupIbu`, `AlamatOrtu`, `KotaOrtu`, `KodePosOrtu`, `PropinsiOrtu`, `NegaraOrtu`, `TeleponOrtu`, `HandphoneOrtu`, `EmailOrtu`, `AsalSekolah`, `AsalSekolah1`, `JenisSekolah_ID`, `KotaSekolah`, `JurusanSekolah`, `NilaiSekolah`, `TahunLulus`, `aktif`, `LulusUjian`, `NilaiUjian`, `GradeNilai`, `TanggalLulus`, `IPK`, `TotalSKS`) VALUES
('125410101', 4, '125410101', 'bc3fd53f1d7f4a67ff0c1341f85e5bd5', '2012', 23, '14032012', 'Rizki Mawardiantoro', 'no foto.jpg', 'B', 'A', '4', '202', '10111321063', 'L', 'WNI', '', 'Bandar Lampung', '1993-01-17', '1', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Perintis kemerdekaan', 'Yogyakarta', '05', '09', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('115410224', 4, '115410224', 'a5de69fd671ee65be32f105d592e9c37', '2011', 23, '14032012', 'Toni Guido', 'no foto.jpg', 'P', 'A', '3', '202', '10111321067', 'L', 'WNI', '', 'Pontianak', '1989-01-14', '2', 'B', '', '', '', '', '', '', '', '', '', '', 'Asrama putra kalbar, gowok', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('105410201', 4, '105410201', '6fb24a2bee0db84acdbec6d7e10db7cb', '2010', 23, '14032012', 'Vahry N Idrus', 'no foto.jpg', 'P', 'A', '3', '202', '10111321064', 'L', 'WNI', '', 'Ternate', '1989-08-12', '1', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Sorowajan baru, Nologaten', 'Yogyakarta', '06', '03', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('125610075', 4, '125610075', 'ece141fcd50753c40e94c128bc035577', '2012', 22, '14032012', 'Andreas Prastya', 'no foto.jpg', 'B', 'A', '3', '201', '10111321066', 'L', 'WNI', '', 'Kota bumi Lampung Utara', '1993-05-18', '1', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl. Taman siswa', 'Yogyakarta', '003', '004', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('115410206', 4, '115410206', 'fac947d4d95d3263e784c45f86536947', '2011', 22, '14032012', 'Dede Brahma Arianto', 'no foto.jpg', 'P', 'A', '3', '202', '10111321062', 'L', 'WNI', '', 'Bandar Lampung', '2015-01-21', '1', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl. Kebun raya no.234', 'Yogyakarta', '001', '002', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('135410206', 4, '135410206', '7958cde3090313b22f38809f98f9fa6a', '2013', 23, '14032012', 'Ria Febriana', 'no foto.jpg', 'B', 'A', '3', '202', '10111321064', 'P', 'WNI', '', 'Bandung', '1991-03-25', '3', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Nologaten', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('135410128', 4, '135410128', '2dae04d59d436f02399a0d9711bac9d9', '2013', 23, '14032012', 'I Made Gusti Wirayuda', 'no foto.jpg', 'B', 'A', '4', '202', '10111321067', 'P', 'WNI', '', 'Bali', '1990-01-29', '5', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.pura', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('105410254', 4, '105410254', '388dfd6f366807870a9d08cc435dccbe', '2010', 23, '14032012', 'Janet Apta Adristy', 'no foto.jpg', 'B', 'A', '4', '202', '10111321063', 'P', 'WNI', '', 'Jakarta', '1988-09-15', '4', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Veteran', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('105410268', 4, '105410268', '53bc6f9cdd85ce39e4a708b1eea8ed78', '2010', 19, '14032012', 'Nadine Chandrawinata', 'no foto.jpg', 'B', 'A', '3', '101', '10111321065', 'P', 'WNA', '', 'Semarang', '1988-11-01', '2', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Gejayan', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('105410203', 4, '105410203', 'ecd0745749518ec902779d9fc21c8102', '2010', 19, '14032012', 'Muhammad Rizky', 'no foto.jpg', 'B', 'A', '3', '101', '10111321066', 'L', 'WNI', '', 'yogyakarta', '1989-12-31', '1', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Raya Janti', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('125410196', 4, '125410196', '6ae949292541967527ae0d35bad0f5fe', '2012', 19, '14032012', 'Alexander Ismail', 'no foto.jpg', 'B', 'A', '4', '101', '10111321065', 'L', 'WNI', '', 'surabaya', '1992-09-09', '3', 'B', '', '', '', '', '', '', '', '', '', '', 'Demangan Baru', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('135412209', 4, '135412209', '9f121b993833edee449fca03987a5abd', '2013', 19, '14032012', 'Gusti Made Ngurah Wijaya', 'no foto.jpg', 'B', 'A', '4', '101', '10111321068', 'L', 'WNI', '', 'Bali', '1993-02-03', '5', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.pura', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('125410200', 4, '125410200', '583682fd3398a87e72f8c92237816fc5', '2012', 19, '14032012', 'Joni Alexander', 'no foto.jpg', 'B', 'A', '3', '101', '10111321066', 'L', 'WNI', '', 'Cirebon', '1991-05-29', '4', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.AM Sangaji', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('105410239', 4, '105410239', 'c4454f257fb7d6cf8c38149c111e75fa', '2010', 20, '14032012', 'Surya Kencana Fahrozi', 'no foto.jpg', 'P', 'A', '3', '102', '10111321062', 'L', 'WNI', '', 'Bogor', '1988-06-11', '4', 'B', '', '', '', '', '', '', '', '', '', '', 'Gambiran', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('115410297', 4, '115410297', '206b5d84998d863375c0bdbb04c94b70', '2011', 20, '14032012', 'Intan Christiani', 'no foto.jpg', 'B', 'A', '4', '102', '10111321066', 'P', 'WNI', '', 'Madura', '1991-11-13', '2', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl. Taman siswa', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('125410267', 4, '125410267', '2c139c018d45815acd2977ccb9c807c0', '2012', 20, '14032012', 'Kadek Wiraguna', 'no foto.jpg', 'B', 'A', '4', '102', '10111321066', 'L', 'WNI', '', 'Palembang', '1992-11-27', '5', 'B', '', '', '', '', '', '', '', '', '', '', 'Babarsari', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('135411303', 4, '135411303', '80561902707c7bbe9a9ba4d7d440f358', '2013', 20, '14032012', 'Auliani Fitri Khasanah', 'no foto.jpg', 'B', 'A', '3', '102', '10111321062', 'P', 'WNI', '', 'Padang', '1993-02-20', '1', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Kanoman', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('115410197', 4, '115410197', '0a374761eb96c0bd1b8e89a53da10b12', '2011', 21, '14032012', 'Zainal Abidin Fakhroni', 'no foto.jpg', 'P', 'A', '3', '103', '10111321062', 'L', 'WNI', '', 'Medan', '1992-02-02', '1', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.pura', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('125410078', 4, '125410078', 'eeaa6df713bff8f4b4509cf091101f9d', '2012', 21, '14032012', 'Asya Syakuru', 'no foto.jpg', 'B', 'A', '4', '103', '10111321068', 'P', 'WNA', '', 'Jakarta', '1991-10-06', '4', 'B', '', '', '', '', '', '', '', '', '', '', 'Godean', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('135410261', 4, '135410261', 'c26afb1d691ed8887543671e184ad57b', '2013', 21, '14032012', 'Leandro Stevanus', 'no foto.jpg', 'B', 'A', '3', '103', '10111321067', 'L', 'WNI', '', 'Ambon', '1993-07-07', '3', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Parangtritis', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('135410006', 4, '135410006', 'abc65a793428e6202508119e9673d46f', '2013', 21, '14032012', 'I Nyoman Gusti', 'no foto.jpg', 'B', 'A', '4', '103', '10111321062', 'P', 'WNI', '', 'Madiun', '1992-08-17', '5', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.pura', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('135610012', 4, '135610012', 'fc93b92d15034b35b84454234831066f', '2013', 22, '14032012', 'Cintya Kumala Anjani', 'no foto.jpg', 'B', 'A', '4', '201', '10111321066', 'P', 'WNI', '', 'Malang', '1992-02-22', '4', 'B', '', '', '', '', '', '', '', '', '', '', 'Senturan', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('125810084', 4, '125810084', '023bebe2fe5dc1b7ea3f8a3b83c801cb', '2012', 22, '14032012', 'Henry Aristya', 'no foto.jpg', 'B', 'A', '4', '201', '10111321066', 'L', 'WNI', '', 'Jakarta', '1992-07-07', '3', 'B', '', '', '', '', '', '', '', '', '', '', 'Jl.Bhayangkara', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0),
('135810072', 4, '135810072', '3888e26f1ed23848cfc5c5fb36364757', '2013', 22, '14032012', 'Sofi Sofiani', 'no foto.jpg', 'P', 'A', '3', '201', '10111321066', 'P', 'WNI', '', 'Jambi', '1993-11-28', '4', 'B', '', '', '', '', '', '', '', '', '', '', 'Asrama putri jambi, Bintaran', 'Yogyakarta', '', '', '', 'Yogyakarta', 'Indonesia', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', NULL, '0', NULL, '0', '', '', '', 'N', 0, NULL, '0000-00-00', 0.00, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_nilai`
--

CREATE TABLE IF NOT EXISTS `master_nilai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipmin` decimal(5,2) NOT NULL,
  `ipmax` decimal(5,2) NOT NULL,
  `MaxSKS` int(3) NOT NULL,
  `Identitas_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Jurusan_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `master_nilai`
--

INSERT INTO `master_nilai` (`id`, `ipmin`, `ipmax`, `MaxSKS`, `Identitas_ID`, `Jurusan_ID`) VALUES
(1, 1.20, 1.69, 12, '14032012', '261'),
(2, 1.70, 2.19, 16, '14032012', '261'),
(3, 2.20, 2.69, 19, '14032012', '261'),
(4, 2.70, 2.99, 21, '14032012', '261'),
(5, 3.00, 4.00, 24, '14032012', '261'),
(6, 0.00, 1.19, 9, '14032012', '261');

-- --------------------------------------------------------

--
-- Struktur dari tabel `matakuliah`
--

CREATE TABLE IF NOT EXISTS `matakuliah` (
  `Matakuliah_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Identitas_ID` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `Kode_mtk` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `Nama_matakuliah` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `Nama_english` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `Semester` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `SKS` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `Jurusan_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `KelompokMtk_ID` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `JenisMTK_ID` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `JenisKurikulum_ID` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `StatusMtk_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Kurikulum_ID` int(11) NOT NULL,
  `Penanggungjawab` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Ket` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`Matakuliah_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=66 ;

--
-- Dumping data untuk tabel `matakuliah`
--

INSERT INTO `matakuliah` (`Matakuliah_ID`, `Identitas_ID`, `Kode_mtk`, `Nama_matakuliah`, `Nama_english`, `Semester`, `SKS`, `Jurusan_ID`, `KelompokMtk_ID`, `JenisMTK_ID`, `JenisKurikulum_ID`, `StatusMtk_ID`, `Kurikulum_ID`, `Penanggungjawab`, `Ket`, `Aktif`) VALUES
(30, '14032012', 'TI401UP', 'Praktikum Analisi Desain Berorientasi Objek', '', '4', '1', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(29, '14032012', 'TI401UG', 'Analisis Desain Berorientasi Objek', '', '4', '3', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(28, '14032012', 'TI302UT', 'Struktur Data', '', '3', '3', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(27, '14032012', 'TI301UP', 'Praktikum Analisis Desain Terstruktur', '', '3', '1', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(26, '14032012', 'TI301UT', 'Analisis Desain Terstruktur', '', '3', '2', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(25, '14032012', 'TI202UT', 'Algoritma Dan Pemrograman 2', '', '2', '2', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(24, '14032012', 'TI201UP', 'Praktikum Sistem Basis Data', '', '2', '1', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(23, '14032012', 'TI201UG', 'Sistem Basis Data', '', '2', '3', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(22, '14032012', 'TI103LT', 'Bahasa Inggris 1', '', '1', '2', '202', 'C', 'W', '0', 'A', 24, '0', '', 'Y'),
(21, '14032012', 'TI101UP', 'Praktikum Algoritma Dan Pemrograman 1', '', '1', '2', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(20, '14032012', 'TI101UT', 'Algoritma Dan Pemrograman 1', '', '1', '3', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(31, '14032012', 'TI402UT', 'Pemrograman Desktop', '', '4', '3', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(32, '14032012', 'TI501UT', 'Bahasa Inggris 2', '', '5', '3', '202', 'C', 'W', '0', 'A', 24, '0', '', 'Y'),
(33, '14032012', 'TI502UT', 'Pemrograman Web', '', '5', '3', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(34, '14032012', 'TI501UP', 'Praktikum Pemrograman Web', '', '5', '1', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(35, '14032012', 'TI601UT', 'Kewirausahaan', '', '6', '2', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(36, '14032012', 'TI602UT', 'Jaringan Saraf Tiruan', '', '6', '3', '202', 'A', 'WM', '0', 'A', 24, '0', '', 'Y'),
(37, '14032012', 'TI603UT', 'Sistem Pakar', '', '6', '3', '202', 'A', 'WM', '0', 'A', 24, '0', '', 'Y'),
(38, '14032012', 'TI701PK', 'Praktik Kerja Lapangan', '', '7', '1', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(39, '14032012', 'TI702UK', 'Proyek Rekayasa Perangkat Lunak', '', '7', '2', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(40, '14032012', 'TI801UK', 'Skripsi', '', '8', '6', '202', 'A', 'W', '0', 'A', 24, '0', '', 'Y'),
(41, '14032012', 'TI508UT', 'Basis Data Oracle', '', '5', '3', '202', 'C', 'P', '0', 'A', 0, '0', '', 'Y'),
(42, '14032012', 'TI609UT', 'Grafika Komputer', '', '6', '2', '202', 'A', 'P', '0', 'A', 24, '0', '', 'Y'),
(43, '14032012', 'TI708UG', 'Keamanan Jaringan', '', '7', '3', '202', 'A', 'P', '0', 'A', 24, '0', '', 'Y'),
(44, '14032012', 'SI101UT', 'Algoritma Dan Pemrograman 1', '', '1', '2', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(45, '14032012', 'SI102UT', 'Bahasa Inggris 1', '', '1', '2', '201', 'C', 'W', '0', 'A', 25, '0', '', 'Y'),
(46, '14032012', 'SI103UT', 'Akuntansi Pengantar', '', '1', '2', '201', 'B', 'W', '0', 'A', 25, '0', '', 'Y'),
(47, '14032012', 'SI201UT', 'Algoritma Dan Pemrograman 2', '', '2', '2', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(48, '14032012', 'SI202UP', 'Praktikum Algoritma Dan Pemrograman 2', '', '2', '1', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(49, '14032012', 'SI203PT', 'Bahasa Inggris 2', '', '2', '2', '201', 'C', 'W', '0', 'A', 25, '0', '', 'Y'),
(50, '14032012', 'SI301UT', 'Jaringan Komputer', '', '3', '2', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(51, '14032012', 'SI301UP', 'Praktikum Jaringan Komputer', '', '3', '1', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(52, '14032012', 'SI303UT', 'Pemrograman Web', '', '3', '2', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(53, '14032012', 'SI401UT', 'Keamanan Sistem Informasi', '', '4', '2', '201', 'C', 'W', '0', 'A', 25, '0', '', 'Y'),
(54, '14032012', 'SI402UT', 'Metode Pengembangan Sistem Informasi', '', '4', '2', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(55, '14032012', 'SI501UT', 'Software Quality Assurance', '', '5', '2', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(56, '14032012', 'SI502UP', 'Praktikum Software Quality Assurance', '', '5', '1', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(57, '14032012', 'SI601UT', 'Audit Sistem Informasi', '', '6', '2', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(58, '14032012', 'SI602UT', 'Sistem Informasi Fungsional', '', '6', '3', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(59, '14032012', 'SI701PK', 'Praktik Kerja Lapangan', '', '7', '1', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(60, '14032012', 'SI702UK', 'Proyek Enterprise Information System', '', '7', '2', '201', 'A', 'W', '0', 'A', 0, '0', '', 'Y'),
(61, '14032012', 'SI801UK', 'Skripsi', '', '8', '6', '201', 'A', 'W', '0', 'A', 25, '0', '', 'Y'),
(62, '14032012', 'SI409UT', 'Akuntansi Keuangan', '', '4', '2', '201', 'C', 'P', '0', 'A', 25, '0', '', 'Y'),
(63, '14032012', 'SI510UP', 'Pengenalan Oracle', '', '5', '2', '201', 'B', 'P', '0', 'A', 25, '0', '', 'Y'),
(64, '14032012', 'SI606UT', 'Basis Data Terdistribusi', '', '6', '3', '201', 'B', 'P', '0', 'A', 25, '0', '', 'Y'),
(65, '14032012', 'SI704UT', 'Data Warehouse', '', '7', '2', '201', 'B', 'P', '0', 'A', 25, '0', '', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menuawalleft`
--

CREATE TABLE IF NOT EXISTS `menuawalleft` (
  `id_menu` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `link` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `menuawalleft`
--

INSERT INTO `menuawalleft` (`id_menu`, `nama`, `link`, `aktif`) VALUES
(1, 'Login', '?page=login', 'Y'),
(2, 'Halaman Depan', '?page=home', 'Y'),
(3, 'Area Download', '', 'Y'),
(4, 'Pendaftaran Mahasiswa Baru', '', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `Nilai_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Identitas_ID` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Kode_Jurusan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `grade` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `bobot` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `NilaiMin` decimal(10,2) NOT NULL,
  `NilaiMax` decimal(10,2) NOT NULL,
  `keterangan` varchar(50) CHARACTER SET koi8u NOT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`Nilai_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`Nilai_ID`, `Identitas_ID`, `Kode_Jurusan`, `grade`, `bobot`, `NilaiMin`, `NilaiMax`, `keterangan`, `Aktif`) VALUES
(1, '14032012', '202', 'E', '0', 0.00, 2.99, 'Buruk Sekali', 'Y'),
(2, '14032012', '202', 'D', '1', 3.00, 4.99, 'Buruk', 'Y'),
(3, '14032012', '202', 'C', '2', 5.00, 6.99, 'Baik', 'Y'),
(4, '14032012', '202', 'B', '3', 7.00, 8.99, 'Baik Sekali', 'Y'),
(5, '14032012', '202', 'A', '4', 9.00, 100.00, 'Istimewa', 'Y'),
(6, '14032012', '263', 'E', '0', 0.00, 2.99, 'Buruk Sekali', 'Y'),
(7, '14032012', '263', 'D', '1', 3.00, 4.99, 'Buruk', 'Y'),
(8, '14032012', '263', 'C', '2', 5.00, 6.99, 'Baik', 'Y'),
(9, '14032012', '263', 'B', '3', 7.00, 8.99, 'Baik Sekali', 'Y'),
(10, '14032012', '263', 'A', '4', 9.00, 100.00, 'Istimewa', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai1`
--

CREATE TABLE IF NOT EXISTS `nilai1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilmin` decimal(17,2) NOT NULL,
  `nilmax` decimal(17,2) NOT NULL,
  `grade` varchar(1) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `nilai1`
--

INSERT INTO `nilai1` (`id`, `nilmin`, `nilmax`, `grade`) VALUES
(1, 85.00, 100.00, 'A'),
(2, 75.00, 84.99, 'B'),
(3, 55.00, 74.99, 'C'),
(4, 35.00, 54.99, 'D'),
(5, 0.00, 34.99, 'E');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pekerjaanortu`
--

CREATE TABLE IF NOT EXISTS `pekerjaanortu` (
  `Pekerjaan` char(3) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NA` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Pekerjaan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `pekerjaanortu`
--

INSERT INTO `pekerjaanortu` (`Pekerjaan`, `Nama`, `NA`) VALUES
('1', 'Pegawai Negeri', 'N'),
('2', 'ABRI', 'N'),
('3', 'Pegawai Swasta', 'N'),
('4', 'Usaha Sendiri', 'N'),
('5', 'Tidak Bekerja', 'N'),
('6', 'Pensiun', 'N'),
('7', 'Lain-lain', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikanortu`
--

CREATE TABLE IF NOT EXISTS `pendidikanortu` (
  `Pendidikan` char(3) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NA` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Pendidikan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `pendidikanortu`
--

INSERT INTO `pendidikanortu` (`Pendidikan`, `Nama`, `NA`) VALUES
('1', 'Tidak Tamat SD', 'N'),
('2', 'Tamat SD', 'N'),
('3', 'Tamat SMP', 'N'),
('4', 'Tamat SMTA', 'N'),
('5', 'Diploma', 'N'),
('6', 'Sarjana Muda', 'N'),
('7', 'Sarjana', 'N'),
('8', 'Pasca Sarjana', 'N'),
('9', 'Doktor', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perhatian`
--

CREATE TABLE IF NOT EXISTS `perhatian` (
  `ID` int(11) NOT NULL,
  `header` text COLLATE latin1_general_ci NOT NULL,
  `t1` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `t2` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `t3` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `t4` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `t5` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `t6` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `gb` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `perhatian`
--

INSERT INTO `perhatian` (`ID`, `header`, `t1`, `t2`, `t3`, `t4`, `t5`, `t6`, `gb`) VALUES
(1, '::.Warning.:: KRS YANG TELAH DI SEND/SUBMIT/KIRIM TDK BISA DIEDIT PASTIKAN SEBELUM DISEND TELITI DULU:', '1. Batas Akhir pengisian Kartu Rencana Studi (KRS) dimulai pada tanggal', '2. Perubahan Kartu Rencana Studi (KRS) tidak akan dilayani jika batas penginputan KRS telah berakhir', '', '', '', '', 'warning.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `Program_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `nama_program` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `Identitas_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `program`
--

INSERT INTO `program` (`ID`, `Program_ID`, `nama_program`, `Identitas_ID`, `aktif`) VALUES
(1, 'REG', 'REGULER', '1308901937', 'Y'),
(2, 'EXEC', 'EXECUTIVE', '1308901937', 'Y'),
(3, 'R', 'REGULER', '14032012', 'Y'),
(4, 'N', 'NON REGULER', '14032012', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `regmhs`
--

CREATE TABLE IF NOT EXISTS `regmhs` (
  `ID_Reg` int(11) NOT NULL AUTO_INCREMENT,
  `Tahun` varchar(6) COLLATE latin1_general_ci NOT NULL,
  `Identitas_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Jurusan_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NIM` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `tgl_reg` date NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID_Reg`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `regmhs`
--

INSERT INTO `regmhs` (`ID_Reg`, `Tahun`, `Identitas_ID`, `Jurusan_ID`, `NIM`, `tgl_reg`, `aktif`) VALUES
(12, '20122', '14032012', '261', '20102611001', '2012-05-04', 'Y'),
(11, '20121', '14032012', '261', '20102611001', '2012-05-03', 'Y'),
(10, '20112', '14032012', '261', '20102611001', '2012-05-03', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE IF NOT EXISTS `ruang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Ruang_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Kampus_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Lantai` smallint(5) unsigned DEFAULT '1',
  `Kode_Jurusan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `RuangKuliah` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'Y',
  `Kapasitas` int(10) unsigned DEFAULT '0',
  `KapasitasUjian` int(10) unsigned DEFAULT '0',
  `Keterangan` text COLLATE latin1_general_ci,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=194 ;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`ID`, `Ruang_ID`, `Nama`, `Kampus_ID`, `Lantai`, `Kode_Jurusan`, `RuangKuliah`, `Kapasitas`, `KapasitasUjian`, `Keterangan`, `Aktif`) VALUES
(24, 'RU-B2', 'B-1.2', 'K1', 1, '101', 'Y', 30, 20, '', 'Y'),
(25, 'RU-B2', 'B-1.2', 'K1', 1, '102', 'Y', 30, 20, '', 'Y'),
(193, 'RU-B1', 'B-1.1', 'K1', 1, '202', 'Y', 30, 20, '', 'Y'),
(192, 'RU-B1', 'B-1.1', 'K1', 1, '201', 'Y', 30, 20, '', 'Y'),
(191, 'RU-B1', 'B-1.1', 'K1', 1, '103', 'Y', 30, 20, '', 'Y'),
(190, 'RU-B1', 'B-1.1', 'K1', 1, '102', 'Y', 30, 20, '', 'Y'),
(189, 'RU-B1', 'B-1.1', 'K1', 1, '101', 'Y', 30, 20, '', 'Y'),
(26, 'RU-B2', 'B-1.2', 'K1', 1, '103', 'Y', 30, 20, '', 'Y'),
(27, 'RU-B2', 'B-1.2', 'K1', 1, '201', 'Y', 30, 20, '', 'Y'),
(28, 'RU-B2', 'B-1.2', 'K1', 1, '202', 'Y', 30, 20, '', 'Y'),
(29, 'RU-B3', 'B-1.3', 'K1', 1, '101', 'Y', 20, 20, '', 'Y'),
(30, 'RU-B3', 'B-1.3', 'K1', 1, '102', 'Y', 20, 20, '', 'Y'),
(31, 'RU-B3', 'B-1.3', 'K1', 1, '103', 'Y', 20, 20, '', 'Y'),
(32, 'RU-B3', 'B-1.3', 'K1', 1, '201', 'Y', 20, 20, '', 'Y'),
(33, 'RU-B3', 'B-1.3', 'K1', 1, '202', 'Y', 20, 20, '', 'Y'),
(34, 'RU-B4', 'B-1.4', 'K1', 1, '101', 'Y', 30, 40, '', 'Y'),
(35, 'RU-B4', 'B-1.4', 'K1', 1, '102', 'Y', 30, 40, '', 'Y'),
(36, 'RU-B4', 'B-1.4', 'K1', 1, '103', 'Y', 30, 40, '', 'Y'),
(37, 'RU-B4', 'B-1.4', 'K1', 1, '201', 'Y', 30, 40, '', 'Y'),
(38, 'RU-B4', 'B-1.4', 'K1', 1, '202', 'Y', 30, 40, '', 'Y'),
(39, 'RU-B5', 'B-1.5', 'K1', 1, '101', 'Y', 30, 40, '', 'Y'),
(40, 'RU-B5', 'B-1.5', 'K1', 1, '102', 'Y', 30, 40, '', 'Y'),
(41, 'RU-B5', 'B-1.5', 'K1', 1, '103', 'Y', 30, 40, '', 'Y'),
(42, 'RU-B5', 'B-1.5', 'K1', 1, '201', 'Y', 30, 40, '', 'Y'),
(43, 'RU-B5', 'B-1.5', 'K1', 1, '202', 'Y', 30, 40, '', 'Y'),
(44, 'RU-S1', 'S-2.1', 'K1', 2, '101', 'Y', 30, 30, '', 'Y'),
(45, 'RU-S1', 'S-2.1', 'K1', 2, '102', 'Y', 30, 30, '', 'Y'),
(46, 'RU-S1', 'S-2.1', 'K1', 2, '103', 'Y', 30, 30, '', 'Y'),
(47, 'RU-S1', 'S-2.1', 'K1', 2, '201', 'Y', 30, 30, '', 'Y'),
(48, 'RU-S1', 'S-2.1', 'K1', 2, '202', 'Y', 30, 30, '', 'Y'),
(54, 'RU-S2', 'S-2.2', 'K1', 2, '101', 'Y', 30, 30, '', 'Y'),
(57, 'RU-S2', 'S-2.2', 'K1', 2, '201', 'Y', 30, 30, '', 'Y'),
(56, 'RU-S2', 'S-2.2', 'K1', 2, '103', 'Y', 30, 30, '', 'Y'),
(55, 'RU-S2', 'S-2.2', 'K1', 2, '102', 'Y', 30, 30, '', 'Y'),
(58, 'RU-S2', 'S-2.2', 'K1', 2, '202', 'Y', 30, 30, '', 'Y'),
(59, 'RU-S3', 'S-2.3', 'K1', 2, '101', 'Y', 30, 30, '', 'Y'),
(60, 'RU-S3', 'S-2.3', 'K1', 2, '102', 'Y', 30, 30, '', 'Y'),
(61, 'RU-S3', 'S-2.3', 'K1', 2, '103', 'Y', 30, 30, '', 'Y'),
(62, 'RU-S3', 'S-2.3', 'K1', 2, '201', 'Y', 30, 30, '', 'Y'),
(63, 'RU-S3', 'S-2.3', 'K1', 2, '202', 'Y', 30, 30, '', 'Y'),
(64, 'RU-S4', 'S-3.1', 'K1', 3, '101', 'Y', 30, 40, '', 'Y'),
(65, 'RU-S4', 'S-3.1', 'K1', 3, '102', 'Y', 30, 40, '', 'Y'),
(66, 'RU-S4', 'S-3.1', 'K1', 3, '103', 'Y', 30, 40, '', 'Y'),
(67, 'RU-S4', 'S-3.1', 'K1', 3, '201', 'Y', 30, 40, '', 'Y'),
(68, 'RU-S4', 'S-3.1', 'K1', 3, '202', 'Y', 30, 40, '', 'Y'),
(69, 'RU-S5', 'S-3.2', 'K1', 3, '101', 'Y', 30, 40, '', 'Y'),
(70, 'RU-S5', 'S-3.2', 'K1', 3, '102', 'Y', 30, 40, '', 'Y'),
(71, 'RU-S5', 'S-3.2', 'K1', 3, '103', 'Y', 30, 40, '', 'Y'),
(72, 'RU-S5', 'S-3.2', 'K1', 3, '201', 'Y', 30, 40, '', 'Y'),
(73, 'RU-S5', 'S-3.2', 'K1', 3, '202', 'Y', 30, 40, '', 'Y'),
(74, 'RU-S6', 'S-3.3', 'K1', 3, '101', 'Y', 30, 40, '', 'Y'),
(75, 'RU-S6', 'S-3.3', 'K1', 3, '102', 'Y', 30, 40, '', 'Y'),
(76, 'RU-S6', 'S-3.3', 'K1', 3, '103', 'Y', 30, 40, '', 'Y'),
(77, 'RU-S6', 'S-3.3', 'K1', 3, '201', 'Y', 30, 40, '', 'Y'),
(78, 'RU-S6', 'S-3.3', 'K1', 3, '202', 'Y', 30, 40, '', 'Y'),
(79, 'RU-U1', 'U-2.1', 'K1', 2, '101', 'Y', 20, 20, '', 'Y'),
(80, 'RU-U1', 'U-2.1', 'K1', 2, '102', 'Y', 20, 20, '', 'Y'),
(81, 'RU-U1', 'U-2.1', 'K1', 2, '103', 'Y', 20, 20, '', 'Y'),
(82, 'RU-U1', 'U-2.1', 'K1', 2, '201', 'Y', 20, 20, '', 'Y'),
(83, 'RU-U1', 'U-2.1', 'K1', 2, '202', 'Y', 20, 20, '', 'Y'),
(84, 'RU-U2', 'U-2.2', 'K1', 2, '101', 'Y', 20, 20, '', 'Y'),
(85, 'RU-U2', 'U-2.2', 'K1', 2, '102', 'Y', 20, 20, '', 'Y'),
(86, 'RU-U2', 'U-2.2', 'K1', 2, '103', 'Y', 20, 20, '', 'Y'),
(87, 'RU-U2', 'U-2.2', 'K1', 2, '201', 'Y', 20, 20, '', 'Y'),
(88, 'RU-U2', 'U-2.2', 'K1', 2, '202', 'Y', 20, 20, '', 'Y'),
(89, 'RU-U3', 'U-2.3', 'K1', 2, '101', 'Y', 20, 20, '', 'Y'),
(90, 'RU-U3', 'U-2.3', 'K1', 2, '102', 'Y', 20, 20, '', 'Y'),
(91, 'RU-U3', 'U-2.3', 'K1', 2, '103', 'Y', 20, 20, '', 'Y'),
(92, 'RU-U3', 'U-2.3', 'K1', 2, '201', 'Y', 20, 20, '', 'Y'),
(93, 'RU-U3', 'U-2.3', 'K1', 2, '202', 'Y', 20, 20, '', 'Y'),
(94, 'RU-U4', 'U-2.4', 'K1', 2, '101', 'Y', 20, 30, '', 'Y'),
(95, 'RU-U4', 'U-2.4', 'K1', 2, '102', 'Y', 20, 30, '', 'Y'),
(96, 'RU-U4', 'U-2.4', 'K1', 2, '103', 'Y', 20, 30, '', 'Y'),
(97, 'RU-U4', 'U-2.4', 'K1', 2, '201', 'Y', 20, 30, '', 'Y'),
(98, 'RU-U4', 'U-2.4', 'K1', 2, '202', 'Y', 20, 30, '', 'Y'),
(99, 'RU-U5', 'U-3.1', 'K1', 3, '101', 'Y', 30, 30, '', 'Y'),
(100, 'RU-U5', 'U-3.1', 'K1', 3, '102', 'Y', 30, 30, '', 'Y'),
(101, 'RU-U5', 'U-3.1', 'K1', 3, '103', 'Y', 30, 30, '', 'Y'),
(102, 'RU-U5', 'U-3.1', 'K1', 3, '201', 'Y', 30, 30, '', 'Y'),
(103, 'RU-U5', 'U-3.1', 'K1', 3, '202', 'Y', 30, 30, '', 'Y'),
(104, 'RU-U6', 'U-3.2', 'K1', 3, '101', 'Y', 30, 30, '', 'Y'),
(105, 'RU-U6', 'U-3.2', 'K1', 3, '102', 'Y', 30, 30, '', 'Y'),
(106, 'RU-U6', 'U-3.2', 'K1', 3, '103', 'Y', 30, 30, '', 'Y'),
(107, 'RU-U6', 'U-3.2', 'K1', 3, '201', 'Y', 30, 30, '', 'Y'),
(108, 'RU-U6', 'U-3.2', 'K1', 3, '202', 'Y', 30, 30, '', 'Y'),
(109, 'RU-U7', 'U-3.3', 'K1', 3, '101', 'Y', 30, 30, '', 'Y'),
(110, 'RU-U7', 'U-3.3', 'K1', 3, '102', 'Y', 30, 30, '', 'Y'),
(111, 'RU-U7', 'U-3.3', 'K1', 3, '103', 'Y', 30, 30, '', 'Y'),
(112, 'RU-U7', 'U-3.3', 'K1', 3, '201', 'Y', 30, 30, '', 'Y'),
(113, 'RU-U7', 'U-3.3', 'K1', 3, '202', 'Y', 30, 30, '', 'Y'),
(114, 'RU-U8', 'U-3.4', 'K1', 3, '101', 'Y', 30, 30, '', 'Y'),
(115, 'RU-U8', 'U-3.4', 'K1', 3, '102', 'Y', 30, 30, '', 'Y'),
(116, 'RU-U8', 'U-3.4', 'K1', 3, '103', 'Y', 30, 30, '', 'Y'),
(117, 'RU-U8', 'U-3.4', 'K1', 3, '201', 'Y', 30, 30, '', 'Y'),
(118, 'RU-U8', 'U-3.4', 'K1', 3, '202', 'Y', 30, 30, '', 'Y'),
(119, 'RU-T1', 'T-3.1', 'K1', 3, '101', 'Y', 30, 20, '', 'Y'),
(120, 'RU-T1', 'T-3.1', 'K1', 3, '102', 'Y', 30, 20, '', 'Y'),
(121, 'RU-T1', 'T-3.1', 'K1', 3, '103', 'Y', 30, 20, '', 'Y'),
(122, 'RU-T1', 'T-3.1', 'K1', 3, '201', 'Y', 30, 20, '', 'Y'),
(123, 'RU-T1', 'T-3.1', 'K1', 3, '202', 'Y', 30, 20, '', 'Y'),
(124, 'RU-T2', 'T-3.2', 'K1', 3, '101', 'Y', 30, 20, '', 'Y'),
(125, 'RU-T2', 'T-3.2', 'K1', 3, '102', 'Y', 30, 20, '', 'Y'),
(126, 'RU-T2', 'T-3.2', 'K1', 3, '103', 'Y', 30, 20, '', 'Y'),
(127, 'RU-T2', 'T-3.2', 'K1', 3, '201', 'Y', 30, 20, '', 'Y'),
(128, 'RU-T2', 'T-3.2', 'K1', 3, '202', 'Y', 30, 20, '', 'Y'),
(129, 'RU-T3', 'T-3.3', 'K1', 3, '101', 'Y', 30, 20, '', 'Y'),
(130, 'RU-T3', 'T-3.3', 'K1', 3, '102', 'Y', 30, 20, '', 'Y'),
(131, 'RU-T3', 'T-3.3', 'K1', 3, '103', 'Y', 30, 20, '', 'Y'),
(132, 'RU-T3', 'T-3.3', 'K1', 3, '201', 'Y', 30, 20, '', 'Y'),
(133, 'RU-T3', 'T-3.3', 'K1', 3, '202', 'Y', 30, 20, '', 'Y'),
(134, 'RU-T4', 'T-3.4', 'K1', 3, '101', 'Y', 30, 30, '', 'Y'),
(135, 'RU-T4', 'T-3.4', 'K1', 3, '102', 'Y', 30, 30, '', 'Y'),
(136, 'RU-T4', 'T-3.4', 'K1', 3, '103', 'Y', 30, 30, '', 'Y'),
(137, 'RU-T4', 'T-3.4', 'K1', 3, '201', 'Y', 30, 30, '', 'Y'),
(138, 'RU-T4', 'T-3.4', 'K1', 3, '202', 'Y', 30, 30, '', 'Y'),
(139, 'RU-T5', 'T-3.5', 'K1', 3, '101', 'Y', 30, 30, '', 'Y'),
(140, 'RU-T5', 'T-3.5', 'K1', 3, '102', 'Y', 30, 30, '', 'Y'),
(141, 'RU-T5', 'T-3.5', 'K1', 3, '103', 'Y', 30, 30, '', 'Y'),
(142, 'RU-T5', 'T-3.5', 'K1', 3, '201', 'Y', 30, 30, '', 'Y'),
(143, 'RU-T5', 'T-3.5', 'K1', 3, '202', 'Y', 30, 30, '', 'Y'),
(144, 'LAB-RL', 'Rangkaian Listrik', 'K1', 2, '101', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(145, 'LAB-RL', 'Rangkaian Listrik', 'K1', 2, '102', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(146, 'LAB-RL', 'Rangkaian Listrik', 'K1', 2, '103', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(147, 'LAB-RL', 'Rangkaian Listrik', 'K1', 2, '201', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(148, 'LAB-RL', 'Rangkaian Listrik', 'K1', 2, '202', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(149, 'LAB-KD', 'Komputer Dasar', 'K1', 2, '101', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(150, 'LAB-KD', 'Komputer Dasar', 'K1', 2, '102', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(151, 'LAB-KD', 'Komputer Dasar', 'K1', 2, '103', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(152, 'LAB-KD', 'Komputer Dasar', 'K1', 2, '201', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(153, 'LAB-KD', 'Komputer Dasar', 'K1', 2, '202', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(154, 'LAB-BD', 'Basis Data', 'K1', 3, '101', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(155, 'LAB-BD', 'Basis Data', 'K1', 3, '102', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(156, 'LAB-BD', 'Basis Data', 'K1', 3, '103', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(157, 'LAB-BD', 'Basis Data', 'K1', 3, '201', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(158, 'LAB-BD', 'Basis Data', 'K1', 3, '202', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(159, 'LAB-JAR', 'Jaringan Komputer', 'K1', 3, '101', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(160, 'LAB-JAR', 'Jaringan Komputer', 'K1', 3, '102', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(161, 'LAB-JAR', 'Jaringan Komputer', 'K1', 3, '103', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(162, 'LAB-JAR', 'Jaringan Komputer', 'K1', 3, '201', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(163, 'LAB-JAR', 'Jaringan Komputer', 'K1', 3, '202', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(164, 'LAB-AKT', 'Akuntansi', 'K1', 2, '101', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(165, 'LAB-AKT', 'Akuntansi', 'K1', 2, '102', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(166, 'LAB-AKT', 'Akuntansi', 'K1', 2, '103', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(167, 'LAB-AKT', 'Akuntansi', 'K1', 2, '201', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(168, 'LAB-AKT', 'Akuntansi', 'K1', 2, '202', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(169, 'LAB-BHS', 'Bahasa', 'K1', 3, '101', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(170, 'LAB-BHS', 'Bahasa', 'K1', 3, '102', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(171, 'LAB-BHS', 'Bahasa', 'K1', 3, '103', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(172, 'LAB-BHS', 'Bahasa', 'K1', 3, '201', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(173, 'LAB-BHS', 'Bahasa', 'K1', 3, '202', 'Y', 30, 20, 'UPT Laboratorium', 'Y'),
(174, 'LAB-PRG', 'Pemrograman', 'K1', 2, '101', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(175, 'LAB-PRG', 'Pemrograman', 'K1', 2, '102', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(176, 'LAB-PRG', 'Pemrograman', 'K1', 2, '103', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(177, 'LAB-PRG', 'Pemrograman', 'K1', 2, '201', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(178, 'LAB-PRG', 'Pemrograman', 'K1', 2, '202', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(179, 'LAB-APL', 'Aplikasi', 'K1', 3, '101', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(180, 'LAB-APL', 'Aplikasi', 'K1', 3, '102', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(181, 'LAB-APL', 'Aplikasi', 'K1', 3, '103', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(182, 'LAB-APL', 'Aplikasi', 'K1', 3, '201', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(183, 'LAB-APL', 'Aplikasi', 'K1', 3, '202', 'Y', 30, 30, 'UPT Laboratorium', 'Y'),
(184, 'LAB-MUL', 'Multimedia', 'K1', 3, '101', 'Y', 20, 20, 'UPT Laboratorium', 'Y'),
(185, 'LAB-MUL', 'Multimedia', 'K1', 3, '102', 'Y', 20, 20, 'UPT Laboratorium', 'Y'),
(186, 'LAB-MUL', 'Multimedia', 'K1', 3, '103', 'Y', 20, 20, 'UPT Laboratorium', 'Y'),
(187, 'LAB-MUL', 'Multimedia', 'K1', 3, '201', 'Y', 20, 20, 'UPT Laboratorium', 'Y'),
(188, 'LAB-MUL', 'Multimedia', 'K1', 3, '202', 'Y', 20, 20, 'UPT Laboratorium', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusakreditasi`
--

CREATE TABLE IF NOT EXISTS `statusakreditasi` (
  `statusakreditasi_ID` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(20) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `statusakreditasi`
--

INSERT INTO `statusakreditasi` (`statusakreditasi_ID`, `nama`) VALUES
('A', 'Akreditasi A'),
('B', 'Akreditasi B'),
('C', 'Akreditasi C');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusaktivitasdsn`
--

CREATE TABLE IF NOT EXISTS `statusaktivitasdsn` (
  `StatusAktivDsn_ID` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`StatusAktivDsn_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `statusaktivitasdsn`
--

INSERT INTO `statusaktivitasdsn` (`StatusAktivDsn_ID`, `Nama`, `Aktif`) VALUES
('A', 'AKTIF MENGAJAR', 'Y'),
('C', 'CUTI', 'Y'),
('K', 'KELUAR', 'Y'),
('M', 'ALMARHUM', 'Y'),
('P', 'PENSIUN', 'Y'),
('S', 'STUDI LANJUT', 'Y'),
('T', 'TUGAS DI INSTANSI LAIN', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusawal`
--

CREATE TABLE IF NOT EXISTS `statusawal` (
  `StatusAwal_ID` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `BeliFormulir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `JalurKhusus` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `TanpaTest` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `Catatan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`StatusAwal_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `statusawal`
--

INSERT INTO `statusawal` (`StatusAwal_ID`, `Nama`, `BeliFormulir`, `JalurKhusus`, `TanpaTest`, `Catatan`, `Aktif`) VALUES
('P', 'Pindahan', 'Y', 'N', 'Y', '', 'Y'),
('B', 'Baru', 'Y', 'N', 'N', NULL, 'Y'),
('S', 'PSSB', 'Y', 'Y', 'Y', 'Untuk siswa SMA berprestasi', 'Y'),
('D', 'Drop-in', 'Y', 'N', 'Y', '', 'Y'),
('A', 'Asing', 'Y', 'Y', 'Y', 'Untuk calon mahasiswa asing', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statuskerja`
--

CREATE TABLE IF NOT EXISTS `statuskerja` (
  `StatusKerja_ID` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Def` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `NA` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`StatusKerja_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `statuskerja`
--

INSERT INTO `statuskerja` (`StatusKerja_ID`, `Nama`, `Def`, `NA`) VALUES
('A', 'Dosen Tetap', 'N', 'N'),
('B', 'Dosen PNS Dipekerjakan', 'N', 'N'),
('C', 'Dosen Honorer PTN', 'N', 'N'),
('D', 'Dosen Honorer Non PTN', 'N', 'N'),
('E', 'Dosen Kontrak', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusmhsw`
--

CREATE TABLE IF NOT EXISTS `statusmhsw` (
  `StatusMhsw_ID` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nilai` smallint(6) NOT NULL DEFAULT '0',
  `Keluar` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `Def` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`StatusMhsw_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `statusmhsw`
--

INSERT INTO `statusmhsw` (`StatusMhsw_ID`, `Nama`, `Nilai`, `Keluar`, `Def`, `Aktif`) VALUES
('A', 'Aktif', 1, 'N', 'N', 'N'),
('C', 'Cuti', 0, 'N', 'N', 'N'),
('P', 'Pasif', 1, 'N', 'Y', 'N'),
('K', 'Keluar', 0, 'Y', 'N', 'N'),
('D', 'Drop-out', 0, 'Y', 'N', 'N'),
('L', 'Lulus', 0, 'Y', 'N', 'N'),
('T', 'Tunggu Ujian', 1, 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusmtk`
--

CREATE TABLE IF NOT EXISTS `statusmtk` (
  `StatusMtk_ID` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `statusmtk`
--

INSERT INTO `statusmtk` (`StatusMtk_ID`, `Nama`, `Aktif`) VALUES
('A', 'AKTIF', 'Y'),
('H', 'HAPUS', 'Y'),
('N', 'NON AKTIF', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statussipil`
--

CREATE TABLE IF NOT EXISTS `statussipil` (
  `StatusSipil` char(3) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`StatusSipil`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `statussipil`
--

INSERT INTO `statussipil` (`StatusSipil`, `Nama`, `Aktif`) VALUES
('B', 'Belum Menikah', 'N'),
('K', 'Menikah', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE IF NOT EXISTS `tahun` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Tahun_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Identitas_ID` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `Jurusan_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Program_ID` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `TglKRSMulai` date DEFAULT NULL,
  `TglKRSSelesai` date DEFAULT NULL,
  `TglUbahKRSMulai` date NOT NULL DEFAULT '0000-00-00',
  `TglUbahKRSSelesai` date NOT NULL DEFAULT '0000-00-00',
  `TglCetakKHS` date NOT NULL DEFAULT '0000-00-00',
  `TglCuti` date DEFAULT NULL,
  `TglMundur` date DEFAULT NULL,
  `TglBayarMulai` date NOT NULL DEFAULT '0000-00-00',
  `TglBayarSelesai` date NOT NULL DEFAULT '0000-00-00',
  `TglAutodebetSelesai` date NOT NULL DEFAULT '0000-00-00',
  `TglAutodebetSelesai2` date NOT NULL DEFAULT '0000-00-00',
  `TglKembaliUangKuliah` date DEFAULT NULL,
  `TglKuliahMulai` date DEFAULT NULL,
  `TglKuliahSelesai` date DEFAULT NULL,
  `TglUTSMulai` date DEFAULT NULL,
  `TglUTSSelesai` date DEFAULT NULL,
  `TglUASMulai` date DEFAULT NULL,
  `TglUASSelesai` date DEFAULT NULL,
  `TglNilaiMulai` date NOT NULL,
  `TglNilaiSelesai` date NOT NULL,
  `HanyaAngkatan` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `ProsesBuka` int(11) NOT NULL DEFAULT '0',
  `ProsesTutup` int(11) NOT NULL DEFAULT '0',
  `TglBuat` datetime DEFAULT NULL,
  `LoginBuat` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TglEdit` datetime DEFAULT NULL,
  `LoginEdit` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Catatan` text COLLATE latin1_general_ci,
  `Aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=23 ;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`ID`, `Tahun_ID`, `Identitas_ID`, `Jurusan_ID`, `Program_ID`, `Nama`, `TglKRSMulai`, `TglKRSSelesai`, `TglUbahKRSMulai`, `TglUbahKRSSelesai`, `TglCetakKHS`, `TglCuti`, `TglMundur`, `TglBayarMulai`, `TglBayarSelesai`, `TglAutodebetSelesai`, `TglAutodebetSelesai2`, `TglKembaliUangKuliah`, `TglKuliahMulai`, `TglKuliahSelesai`, `TglUTSMulai`, `TglUTSSelesai`, `TglUASMulai`, `TglUASSelesai`, `TglNilaiMulai`, `TglNilaiSelesai`, `HanyaAngkatan`, `ProsesBuka`, `ProsesTutup`, `TglBuat`, `LoginBuat`, `TglEdit`, `LoginEdit`, `Catatan`, `Aktif`) VALUES
(11, '2014-1', '14032012', '102', '3', 'Semester Gasal 2014/2015', '2014-08-25', '2014-09-03', '0000-00-00', '0000-00-00', '2014-09-05', NULL, NULL, '2014-07-21', '2014-08-18', '0000-00-00', '0000-00-00', NULL, '2014-09-08', '2014-12-26', '2014-10-27', '2014-11-07', '2015-01-05', '2015-01-23', '2015-01-26', '2015-02-06', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(10, '2014-1', '14032012', '101', '3', 'Semester Gasal 2014/2015', '2014-08-25', '2014-09-03', '0000-00-00', '0000-00-00', '2014-09-05', NULL, NULL, '2014-07-21', '2014-08-18', '0000-00-00', '0000-00-00', NULL, '2014-09-08', '2014-12-26', '2014-10-27', '2014-11-07', '2015-01-05', '2015-01-23', '2015-01-26', '2015-02-06', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(5, '2014-1', '14032012', '202', '3', 'Semester Gasal 2014/2015', '2014-08-25', '2014-09-03', '0000-00-00', '0000-00-00', '2014-09-05', NULL, NULL, '2014-07-21', '2014-08-18', '0000-00-00', '0000-00-00', NULL, '2014-09-08', '2014-12-26', '2014-10-27', '2014-11-07', '2015-01-05', '2015-01-23', '2015-01-26', '2015-02-06', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(9, '2014-1', '14032012', '201', '3', 'Semester Gasal 2014/2015', '2014-08-25', '2014-09-03', '0000-00-00', '0000-00-00', '2014-09-05', NULL, NULL, '2014-07-21', '2014-08-18', '0000-00-00', '0000-00-00', NULL, '2014-09-08', '2014-12-26', '2014-10-27', '2014-11-07', '2015-01-05', '2015-01-23', '2015-01-26', '2015-02-06', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(8, '2014-2', '14032012', '202', '3', 'Semester Genap 2014/2015', '2015-02-23', '2015-03-04', '0000-00-00', '0000-00-00', '2015-03-05', NULL, NULL, '2015-01-19', '2015-02-13', '0000-00-00', '0000-00-00', NULL, '2015-03-09', '2015-06-26', '2015-04-27', '2015-05-08', '2015-07-29', '2015-08-07', '2015-08-10', '2015-08-19', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(12, '2014-1', '14032012', '103', '3', 'Semester Gasal 2014/2015', '2014-08-25', '2014-09-03', '0000-00-00', '0000-00-00', '2014-09-05', NULL, NULL, '2014-07-21', '2014-08-18', '0000-00-00', '0000-00-00', NULL, '2014-09-08', '2014-12-26', '2014-10-27', '2014-11-07', '2015-01-05', '2015-01-23', '2015-01-26', '2015-02-06', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(13, '2014-2', '14032012', '201', '3', 'Semester Genap 2014/2015', '2015-02-23', '2015-03-04', '0000-00-00', '0000-00-00', '2015-03-05', NULL, NULL, '2015-01-19', '2015-02-13', '0000-00-00', '0000-00-00', NULL, '2015-03-09', '2015-06-26', '2015-04-27', '2015-05-08', '2015-07-29', '2015-08-07', '2015-08-10', '2015-08-19', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(14, '2014-2', '14032012', '101', '3', 'Semester Genap 2014/2015', '2015-02-23', '2015-03-04', '0000-00-00', '0000-00-00', '2015-03-05', NULL, NULL, '2015-01-19', '2015-02-13', '0000-00-00', '0000-00-00', NULL, '2015-03-09', '2015-06-26', '2015-04-27', '2015-05-08', '2015-07-29', '2015-08-07', '2015-08-10', '2015-08-19', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(15, '2014-2', '14032012', '102', '3', 'Semester Genap 2014/2015', '2015-02-23', '2015-03-04', '0000-00-00', '0000-00-00', '2015-03-05', NULL, NULL, '2015-01-19', '2015-02-13', '0000-00-00', '0000-00-00', NULL, '2015-03-09', '2015-06-26', '2015-04-27', '2015-05-08', '2015-07-29', '2015-08-07', '2015-08-10', '2015-08-19', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(16, '2014-2', '14032012', '103', '3', 'Semester Genap 2014/2015', '2015-02-23', '2015-03-04', '0000-00-00', '0000-00-00', '2015-03-05', NULL, NULL, '2015-01-19', '2015-02-13', '0000-00-00', '0000-00-00', NULL, '2015-03-09', '2015-06-26', '2015-04-27', '2015-05-08', '2015-07-29', '2015-08-07', '2015-08-10', '2015-08-19', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(17, '2013-2', '14032012', '202', '3', 'Semester Genap 2013/2014', '2014-02-20', '2014-02-27', '0000-00-00', '0000-00-00', '2014-02-28', NULL, NULL, '2014-01-20', '2014-02-17', '0000-00-00', '0000-00-00', NULL, '2014-03-03', '2014-06-20', '2014-04-21', '2014-05-02', '2014-07-01', '2014-07-11', '2014-07-14', '2014-08-01', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(18, '2013-2', '14032012', '201', '3', 'Semester Genap 2013/2014', '2014-02-20', '2014-02-27', '0000-00-00', '0000-00-00', '2014-02-28', NULL, NULL, '2014-01-20', '2014-02-17', '0000-00-00', '0000-00-00', NULL, '2014-03-03', '2014-06-20', '2014-04-21', '2014-05-02', '2014-07-01', '2014-07-11', '2014-07-14', '2014-08-01', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(19, '2013-2', '14032012', '101', '3', 'Semester Genap 2013/2014', '2014-02-20', '2014-02-27', '0000-00-00', '0000-00-00', '2014-02-28', NULL, NULL, '2014-01-20', '2014-02-17', '0000-00-00', '0000-00-00', NULL, '2014-03-03', '2014-06-20', '2014-04-21', '2014-05-02', '2014-07-01', '2014-07-11', '2014-07-14', '2014-08-01', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(20, '2013-2', '14032012', '102', '3', 'Semester Genap 2013/2014', '2014-02-20', '2014-02-27', '0000-00-00', '0000-00-00', '2014-02-28', NULL, NULL, '2014-01-20', '2014-02-17', '0000-00-00', '0000-00-00', NULL, '2014-03-03', '2014-06-20', '2014-04-21', '2014-05-02', '2014-07-01', '2014-07-11', '2014-07-14', '2014-08-01', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y'),
(21, '2013-2', '14032012', '103', '3', 'Semester Genap 2013/2014', '2014-02-20', '2014-02-27', '0000-00-00', '0000-00-00', '2014-02-28', NULL, NULL, '2014-01-20', '2014-02-17', '0000-00-00', '0000-00-00', NULL, '2014-03-03', '2014-06-20', '2014-04-21', '2014-05-02', '2014-07-01', '2014-07-11', '2014-07-14', '2014-08-01', NULL, 0, 0, NULL, NULL, NULL, NULL, '', 'Y');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ajar_dosen`
--
CREATE TABLE IF NOT EXISTS `view_ajar_dosen` (
`Jadwal_ID` bigint(20)
,`Tahun_ID` varchar(10)
,`Identitas_ID` varchar(20)
,`Program_ID` varchar(10)
,`Kode_Mtk` varchar(10)
,`Kode_Jurusan` varchar(10)
,`nama_jurusan` varchar(100)
,`Ruang_ID` varchar(10)
,`Kelas` varchar(20)
,`Dosen_ID` int(11)
,`Hari` varchar(10)
,`Jam_Mulai` time
,`Jam_Selesai` time
,`UTSTgl` date
,`UTSHari` varchar(10)
,`UTSMulai` time
,`UTSSelesai` time
,`UTSRuang` varchar(10)
,`UASTgl` date
,`UASHari` varchar(10)
,`UASMulai` time
,`UASSelesai` time
,`UASRuang` varchar(10)
,`JumlahMhsw` int(11)
,`Kapasitas` int(11)
,`Aktif` enum('Y','N')
,`username` varchar(11)
,`password` varchar(100)
,`nama_matakuliah` varchar(100)
,`semester` varchar(2)
,`sks` varchar(2)
,`nama_lengkap` varchar(100)
,`gelar` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_form_mhs`
--
CREATE TABLE IF NOT EXISTS `view_form_mhs` (
`ID` int(11)
,`tahun` varchar(10)
,`Identitas_ID` varchar(10)
,`kode_jurusan` varchar(20)
,`Program_ID` varchar(20)
,`Aktif` enum('Y','N')
,`tahun_akd` varchar(6)
,`NIM` varchar(15)
,`nama_lengkap` varchar(100)
,`nama_jurusan` varchar(100)
,`kode_prog` varchar(10)
,`nama_program` varchar(100)
,`pembimbing` varchar(100)
,`Gelar` varchar(50)
,`Foto` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_form_mhsakademik`
--
CREATE TABLE IF NOT EXISTS `view_form_mhsakademik` (
`level_ID` int(11)
,`NIM` varchar(50)
,`nama_lengkap` varchar(100)
,`identitas_ID` varchar(10)
,`kode_jurusan` varchar(20)
,`nama_jurusan` varchar(100)
,`ID` int(2)
,`Program_ID` varchar(10)
,`nama_program` varchar(100)
,`pembimbing` varchar(100)
,`Gelar` varchar(50)
,`foto` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ipk`
--
CREATE TABLE IF NOT EXISTS `view_ipk` (
`tahun` varchar(10)
,`NIM` varchar(20)
,`kode_mtk` varchar(10)
,`nama_matakuliah` varchar(100)
,`dosen` varchar(100)
,`SKS` varchar(2)
,`semester` varchar(2)
,`nilai_tgs` decimal(16,2)
,`nilai_akhir` decimal(17,2)
,`GradeNilai` varchar(10)
,`BobotNilai` decimal(4,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jadwal`
--
CREATE TABLE IF NOT EXISTS `view_jadwal` (
`Jadwal_ID` bigint(20)
,`Tahun_ID` varchar(10)
,`Tahun` varchar(10)
,`Identitas_ID` varchar(20)
,`Program_ID` varchar(10)
,`Kode_Mtk` varchar(10)
,`Kode_Jurusan` varchar(10)
,`Ruang_ID` varchar(10)
,`NamaRuang` varchar(50)
,`NR` varchar(50)
,`Kelas` varchar(20)
,`Dosen_ID` int(11)
,`Hari` varchar(10)
,`Jam_Mulai` time
,`Jam_Selesai` time
,`SKS` varchar(2)
,`Aktif` enum('Y','N')
,`UASHari` varchar(10)
,`UASMulai` time
,`UASRuang` varchar(10)
,`UASSelesai` time
,`UASTgl` date
,`UTSHari` varchar(10)
,`UTSMulai` time
,`UTSRuang` varchar(10)
,`UTSSelesai` time
,`UTSTgl` date
,`Nama_matakuliah` varchar(100)
,`nama_lengkap` varchar(100)
,`Gelar` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jadwalkrs_akademik`
--
CREATE TABLE IF NOT EXISTS `view_jadwalkrs_akademik` (
`id` bigint(20)
,`idtahun` varchar(10)
,`nama_program` varchar(100)
,`tahun` varchar(10)
,`kode_mtk` varchar(10)
,`prodi` varchar(10)
,`kode_jurusan` varchar(10)
,`sks` varchar(2)
,`ruang` varchar(10)
,`kelas` varchar(20)
,`hari` varchar(10)
,`jam_mulai` time
,`jam_selesai` time
,`id_dosen` int(11)
,`nama_lengkap` varchar(100)
,`Gelar` varchar(50)
,`nama_matakuliah` varchar(100)
,`semester` varchar(2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mahasiswa`
--
CREATE TABLE IF NOT EXISTS `view_mahasiswa` (
`NIM` varchar(50)
,`level_ID` int(11)
,`username` varchar(9)
,`password` varchar(100)
,`Angkatan` varchar(8)
,`Kurikulum_ID` int(11)
,`identitas_ID` varchar(10)
,`Nama` varchar(100)
,`Foto` varchar(255)
,`StatusAwal_ID` varchar(5)
,`StatusMhsw_ID` varchar(5)
,`IDProg` varchar(50)
,`kode_jurusan` varchar(20)
,`Kelamin` char(3)
,`WargaNegara` char(3)
,`Kebangsaan` varchar(50)
,`TempatLahir` varchar(50)
,`TanggalLahir` date
,`Agama` char(2)
,`StatusSipil` char(2)
,`Alamat` varchar(255)
,`Kota` varchar(50)
,`RT` varchar(10)
,`RW` varchar(10)
,`KodePos` varchar(50)
,`Propinsi` varchar(50)
,`Negara` varchar(50)
,`Telepon` varchar(50)
,`Handphone` varchar(50)
,`Email` varchar(100)
,`AlamatAsal` varchar(255)
,`KotaAsal` varchar(50)
,`RTAsal` varchar(10)
,`RWAsal` varchar(10)
,`KodePosAsal` varchar(50)
,`PropinsiAsal` varchar(50)
,`NegaraAsal` varchar(50)
,`NamaAyah` varchar(50)
,`AgamaAyah` char(2)
,`PendidikanAyah` varchar(5)
,`PekerjaanAyah` varchar(5)
,`HidupAyah` varchar(5)
,`NamaIbu` varchar(50)
,`AgamaIbu` char(2)
,`PendidikanIbu` varchar(5)
,`PekerjaanIbu` varchar(5)
,`HidupIbu` varchar(5)
,`AlamatOrtu` varchar(255)
,`KotaOrtu` varchar(50)
,`KodePosOrtu` varchar(50)
,`PropinsiOrtu` varchar(50)
,`NegaraOrtu` varchar(50)
,`TeleponOrtu` varchar(50)
,`HandphoneOrtu` varchar(50)
,`EmailOrtu` varchar(100)
,`AsalSekolah` varchar(50)
,`AsalSekolah1` varchar(50)
,`JenisSekolah_ID` varchar(20)
,`KotaSekolah` varchar(50)
,`JurusanSekolah` varchar(50)
,`NilaiSekolah` varchar(10)
,`TahunLulus` varchar(10)
,`aktif` enum('Y','N')
,`LulusUjian` enum('Y','N')
,`NilaiUjian` float unsigned
,`GradeNilai` varchar(5)
,`TanggalLulus` date
,`IPK` decimal(4,2)
,`TotalSKS` int(11)
,`nama_jurusan` varchar(100)
,`Program_ID` varchar(10)
,`pa` varchar(50)
,`npa` varchar(100)
,`Gelar` varchar(50)
);
-- --------------------------------------------------------

--
-- Structure for view `cetakmtk`
--
DROP TABLE IF EXISTS `cetakmtk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cetakmtk` AS select `t1`.`Matakuliah_ID` AS `Matakuliah_ID`,`t1`.`Identitas_ID` AS `Identitas_ID`,`t1`.`Kode_mtk` AS `Kode_mtk`,`t1`.`Nama_matakuliah` AS `Nama_matakuliah`,`t1`.`Semester` AS `Semester`,`t1`.`SKS` AS `SKS`,`t1`.`Jurusan_ID` AS `Jurusan_ID`,`t1`.`JenisMTK_ID` AS `JenisMTK_ID`,`t1`.`JenisKurikulum_ID` AS `JenisKurikulum_ID`,`t1`.`StatusMtk_ID` AS `StatusMtk_ID`,`t1`.`Penanggungjawab` AS `Penanggungjawab`,`t1`.`Aktif` AS `Aktif`,`t2`.`Nama` AS `NamaSMk`,`t3`.`Nama` AS `NamaJMK` from ((`matakuliah` `t1` left join `statusmtk` `t2` on((`t1`.`StatusMtk_ID` = `t2`.`StatusMtk_ID`))) join `jenismk` `t3`) where ((`t1`.`JenisMTK_ID` = `t3`.`JenisMK_ID`) and (`t1`.`Aktif` = _latin1'Y') and (`t1`.`StatusMtk_ID` = _latin1'A') and (`t1`.`JenisMTK_ID` <> _latin1'B')) group by `t1`.`Matakuliah_ID` order by `t1`.`Matakuliah_ID`,`t1`.`Semester`;

-- --------------------------------------------------------

--
-- Structure for view `coba`
--
DROP TABLE IF EXISTS `coba`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coba` AS select `t1`.`Identitas_ID` AS `Identitas_ID`,`t2`.`Nama_Identitas` AS `Nama_Identitas`,`t1`.`kode_jurusan` AS `kode_jurusan`,`t1`.`nama_jurusan` AS `nama_jurusan`,`t1`.`jenjang` AS `jenjang` from (`jurusan` `t1` join `identitas` `t2`) where (`t1`.`Identitas_ID` = `t2`.`Identitas_ID`);

-- --------------------------------------------------------

--
-- Structure for view `krs1`
--
DROP TABLE IF EXISTS `krs1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `krs1` AS select `t2`.`KRS_ID` AS `id`,`t3`.`Jadwal_ID` AS `idjdwl`,`t1`.`identitas_ID` AS `identitas_ID`,`t6`.`Program_ID` AS `idprog`,`t8`.`nama_program` AS `nama_program`,`t1`.`kode_jurusan` AS `kode_jurusan`,`t9`.`nama_jurusan` AS `nama_jurusan`,`t2`.`Tahun_ID` AS `tahun`,`t6`.`ID` AS `idtahun`,`t6`.`TglCetakKHS` AS `TglCetakKHS`,`t6`.`Aktif` AS `Aktif`,`t1`.`NIM` AS `NIM`,`t1`.`Nama` AS `nama_lengkap`,`t3`.`Ruang_ID` AS `Ruang_ID`,`t3`.`Kelas` AS `kelas`,`t3`.`Kode_Mtk` AS `kode_mtk`,`t4`.`Nama_matakuliah` AS `nama_matakuliah`,`t4`.`Semester` AS `semester`,`t3`.`Hari` AS `hari`,`t3`.`Jam_Mulai` AS `jam_mulai`,`t3`.`Jam_Selesai` AS `jam_selesai`,`t4`.`SKS` AS `sks`,`t2`.`Tugas1` AS `tugas1`,`t2`.`Tugas2` AS `tugas2`,`t2`.`Tugas3` AS `tugas3`,`t2`.`Tugas4` AS `tugas4`,`t2`.`UTS` AS `nilai_mid`,`t2`.`UAS` AS `nilai_uas`,`t3`.`UTSTgl` AS `UTSTgl`,`t3`.`UTSMulai` AS `UTSMulai`,`t3`.`UTSSelesai` AS `UTSSelesai`,`t3`.`UTSRuang` AS `UTSRuang`,`t3`.`UASTgl` AS `UASTgl`,`t3`.`UASMulai` AS `UASMulai`,`t3`.`UASSelesai` AS `UASSelesai`,`t3`.`UASRuang` AS `UASRuang`,round(((((`t2`.`Tugas1` + `t2`.`Tugas2`) + `t2`.`Tugas3`) + `t2`.`Tugas4`) / 40),2) AS `nilai_tgs`,round((((`t2`.`UTS` * 0.4) + (`t2`.`UAS` * 0.5)) + ((((`t2`.`Tugas1` + `t2`.`Tugas2`) + `t2`.`Tugas3`) + `t2`.`Tugas4`) / 40)),2) AS `nilai_akhir`,`t2`.`GradeNilai` AS `GradeNilai`,`t7`.`bobot` AS `bobot`,`t5`.`nama_lengkap` AS `dosen`,`t5`.`Gelar` AS `gelar` from ((((((((`krs` `t2` left join `nilai` `t7` on((`t2`.`GradeNilai` = `t7`.`grade`))) join `mahasiswa` `t1`) join `matakuliah` `t4`) join `tahun` `t6`) join `jadwal` `t3`) left join `dosen` `t5` on((`t3`.`Dosen_ID` = `t5`.`dosen_ID`))) join `program` `t8`) join `jurusan` `t9`) where ((`t3`.`Kode_Jurusan` = `t9`.`kode_jurusan`) and (`t3`.`Program_ID` = `t8`.`ID`) and (`t2`.`Jadwal_ID` = `t3`.`Jadwal_ID`) and (`t3`.`Kode_Mtk` = `t4`.`Kode_mtk`) and (`t1`.`NIM` = `t2`.`NIM`) and (`t6`.`Identitas_ID` = `t1`.`identitas_ID`) and (`t6`.`Jurusan_ID` = `t1`.`kode_jurusan`) and (`t6`.`Tahun_ID` = `t2`.`Tahun_ID`)) group by `t2`.`KRS_ID` order by `t4`.`Semester`,`t1`.`NIM`;

-- --------------------------------------------------------

--
-- Structure for view `view_ajar_dosen`
--
DROP TABLE IF EXISTS `view_ajar_dosen`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ajar_dosen` AS select `t1`.`Jadwal_ID` AS `Jadwal_ID`,`t1`.`Tahun_ID` AS `Tahun_ID`,`t1`.`Identitas_ID` AS `Identitas_ID`,`t1`.`Program_ID` AS `Program_ID`,`t1`.`Kode_Mtk` AS `Kode_Mtk`,`t1`.`Kode_Jurusan` AS `Kode_Jurusan`,`t4`.`nama_jurusan` AS `nama_jurusan`,`t1`.`Ruang_ID` AS `Ruang_ID`,`t1`.`Kelas` AS `Kelas`,`t1`.`Dosen_ID` AS `Dosen_ID`,`t1`.`Hari` AS `Hari`,`t1`.`Jam_Mulai` AS `Jam_Mulai`,`t1`.`Jam_Selesai` AS `Jam_Selesai`,`t1`.`UTSTgl` AS `UTSTgl`,`t1`.`UTSHari` AS `UTSHari`,`t1`.`UTSMulai` AS `UTSMulai`,`t1`.`UTSSelesai` AS `UTSSelesai`,`t1`.`UTSRuang` AS `UTSRuang`,`t1`.`UASTgl` AS `UASTgl`,`t1`.`UASHari` AS `UASHari`,`t1`.`UASMulai` AS `UASMulai`,`t1`.`UASSelesai` AS `UASSelesai`,`t1`.`UASRuang` AS `UASRuang`,`t1`.`JumlahMhsw` AS `JumlahMhsw`,`t1`.`Kapasitas` AS `Kapasitas`,`t1`.`Aktif` AS `Aktif`,`t3`.`username` AS `username`,`t3`.`password` AS `password`,`t2`.`Nama_matakuliah` AS `nama_matakuliah`,`t2`.`Semester` AS `semester`,`t2`.`SKS` AS `sks`,`t3`.`nama_lengkap` AS `nama_lengkap`,`t3`.`Gelar` AS `gelar` from ((((`jadwal` `t1` join `matakuliah` `t2`) join `dosen` `t3`) join `jurusan` `t4`) join `tahun` `t5`) where ((`t1`.`Kode_Mtk` = `t2`.`Kode_mtk`) and (`t1`.`Dosen_ID` = `t3`.`dosen_ID`) and (`t1`.`Kode_Jurusan` = `t4`.`kode_jurusan`) and (`t5`.`ID` = `t1`.`Tahun_ID`) and (`t5`.`Aktif` = _latin1'Y')) group by `t1`.`Jadwal_ID` order by `t1`.`Dosen_ID`;

-- --------------------------------------------------------

--
-- Structure for view `view_form_mhs`
--
DROP TABLE IF EXISTS `view_form_mhs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_form_mhs` AS select `t2`.`ID_Reg` AS `ID`,`t1`.`Tahun_ID` AS `tahun`,`t1`.`Identitas_ID` AS `Identitas_ID`,`t1`.`Jurusan_ID` AS `kode_jurusan`,`t1`.`Program_ID` AS `Program_ID`,`t1`.`Aktif` AS `Aktif`,`t2`.`Tahun` AS `tahun_akd`,`t2`.`NIM` AS `NIM`,`t3`.`Nama` AS `nama_lengkap`,`t4`.`nama_jurusan` AS `nama_jurusan`,`t5`.`Program_ID` AS `kode_prog`,`t5`.`nama_program` AS `nama_program`,`t6`.`nama_lengkap` AS `pembimbing`,`t6`.`Gelar` AS `Gelar`,`t3`.`Foto` AS `Foto` from (((((`tahun` `t1` left join `regmhs` `t2` on(((`t1`.`Tahun_ID` = `t2`.`Tahun`) and (`t1`.`Jurusan_ID` = `t2`.`Jurusan_ID`)))) join `mahasiswa` `t3`) left join `dosen` `t6` on((`t3`.`PenasehatAkademik` = `t6`.`NIDN`))) join `jurusan` `t4`) join `program` `t5`) where ((`t2`.`NIM` = `t3`.`NIM`) and (`t2`.`Jurusan_ID` = `t4`.`kode_jurusan`) and (`t1`.`Identitas_ID` = `t2`.`Identitas_ID`) and (`t3`.`identitas_ID` = `t5`.`Identitas_ID`)) group by `t2`.`ID_Reg`;

-- --------------------------------------------------------

--
-- Structure for view `view_form_mhsakademik`
--
DROP TABLE IF EXISTS `view_form_mhsakademik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_form_mhsakademik` AS select `t1`.`level_ID` AS `level_ID`,`t1`.`NIM` AS `NIM`,`t1`.`Nama` AS `nama_lengkap`,`t1`.`identitas_ID` AS `identitas_ID`,`t1`.`kode_jurusan` AS `kode_jurusan`,`t2`.`nama_jurusan` AS `nama_jurusan`,`t3`.`ID` AS `ID`,`t3`.`Program_ID` AS `Program_ID`,`t3`.`nama_program` AS `nama_program`,`t4`.`nama_lengkap` AS `pembimbing`,`t4`.`Gelar` AS `Gelar`,`t1`.`Foto` AS `foto` from (((`mahasiswa` `t1` left join `dosen` `t4` on((`t1`.`PenasehatAkademik` = `t4`.`NIDN`))) join `jurusan` `t2`) join `program` `t3`) where ((`t1`.`kode_jurusan` = `t2`.`kode_jurusan`) and (`t1`.`IDProg` = `t3`.`ID`)) group by `t1`.`NIM`;

-- --------------------------------------------------------

--
-- Structure for view `view_ipk`
--
DROP TABLE IF EXISTS `view_ipk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ipk` AS select `t1`.`Tahun_ID` AS `tahun`,`t1`.`NIM` AS `NIM`,`t2`.`Kode_Mtk` AS `kode_mtk`,`t4`.`Nama_matakuliah` AS `nama_matakuliah`,`t3`.`nama_lengkap` AS `dosen`,`t4`.`SKS` AS `SKS`,`t4`.`Semester` AS `semester`,round(((((`t1`.`Tugas1` + `t1`.`Tugas2`) + `t1`.`Tugas3`) + `t1`.`Tugas4`) / 40),2) AS `nilai_tgs`,round((((`t1`.`UTS` * 0.4) + (`t1`.`UAS` * 0.5)) + ((((`t1`.`Tugas1` + `t1`.`Tugas2`) + `t1`.`Tugas3`) + `t1`.`Tugas4`) / 40)),2) AS `nilai_akhir`,`t1`.`GradeNilai` AS `GradeNilai`,`t1`.`BobotNilai` AS `BobotNilai` from (((`krs` `t1` join `jadwal` `t2`) join `dosen` `t3`) join `matakuliah` `t4`) where ((`t1`.`Jadwal_ID` = `t2`.`Jadwal_ID`) and (`t2`.`Kode_Mtk` = `t4`.`Kode_mtk`) and (`t2`.`Dosen_ID` = `t3`.`dosen_ID`)) order by `t4`.`Semester`,`t1`.`NIM`;

-- --------------------------------------------------------

--
-- Structure for view `view_jadwal`
--
DROP TABLE IF EXISTS `view_jadwal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jadwal` AS select `t1`.`Jadwal_ID` AS `Jadwal_ID`,`t1`.`Tahun_ID` AS `Tahun_ID`,`t5`.`Tahun_ID` AS `Tahun`,`t1`.`Identitas_ID` AS `Identitas_ID`,`t1`.`Program_ID` AS `Program_ID`,`t1`.`Kode_Mtk` AS `Kode_Mtk`,`t1`.`Kode_Jurusan` AS `Kode_Jurusan`,`t1`.`Ruang_ID` AS `Ruang_ID`,`t4`.`Nama` AS `NamaRuang`,`t4`.`Nama` AS `NR`,`t1`.`Kelas` AS `Kelas`,`t1`.`Dosen_ID` AS `Dosen_ID`,`t1`.`Hari` AS `Hari`,`t1`.`Jam_Mulai` AS `Jam_Mulai`,`t1`.`Jam_Selesai` AS `Jam_Selesai`,`t2`.`SKS` AS `SKS`,`t1`.`Aktif` AS `Aktif`,`t1`.`UASHari` AS `UASHari`,`t1`.`UASMulai` AS `UASMulai`,`t1`.`UASRuang` AS `UASRuang`,`t1`.`UASSelesai` AS `UASSelesai`,`t1`.`UASTgl` AS `UASTgl`,`t1`.`UTSHari` AS `UTSHari`,`t1`.`UTSMulai` AS `UTSMulai`,`t1`.`UTSRuang` AS `UTSRuang`,`t1`.`UTSSelesai` AS `UTSSelesai`,`t1`.`UTSTgl` AS `UTSTgl`,`t2`.`Nama_matakuliah` AS `Nama_matakuliah`,`t3`.`nama_lengkap` AS `nama_lengkap`,`t3`.`Gelar` AS `Gelar` from ((((`jadwal` `t1` join `matakuliah` `t2`) left join `dosen` `t3` on((`t1`.`Dosen_ID` = `t3`.`dosen_ID`))) join `ruang` `t4`) join `tahun` `t5`) where ((`t1`.`Kode_Mtk` = `t2`.`Kode_mtk`) and (`t1`.`Ruang_ID` = `t4`.`ID`) and (`t1`.`Tahun_ID` = `t5`.`ID`)) group by `t1`.`Jadwal_ID` order by `t1`.`Kelas`;

-- --------------------------------------------------------

--
-- Structure for view `view_jadwalkrs_akademik`
--
DROP TABLE IF EXISTS `view_jadwalkrs_akademik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jadwalkrs_akademik` AS select `t4`.`Jadwal_ID` AS `id`,`t4`.`Tahun_ID` AS `idtahun`,`t1`.`nama_program` AS `nama_program`,`t7`.`Tahun_ID` AS `tahun`,`t4`.`Kode_Mtk` AS `kode_mtk`,`t4`.`Program_ID` AS `prodi`,`t4`.`Kode_Jurusan` AS `kode_jurusan`,`t5`.`SKS` AS `sks`,`t4`.`Ruang_ID` AS `ruang`,`t4`.`Kelas` AS `kelas`,`t4`.`Hari` AS `hari`,`t4`.`Jam_Mulai` AS `jam_mulai`,`t4`.`Jam_Selesai` AS `jam_selesai`,`t4`.`Dosen_ID` AS `id_dosen`,`t8`.`nama_lengkap` AS `nama_lengkap`,`t8`.`Gelar` AS `Gelar`,`t5`.`Nama_matakuliah` AS `nama_matakuliah`,`t5`.`Semester` AS `semester` from (((((`mahasiswa` `t2` join `jurusan` `t3`) join `matakuliah` `t5`) join `tahun` `t7`) join (`program` `t1` join `jadwal` `t4`)) left join `dosen` `t8` on((`t4`.`Dosen_ID` = `t8`.`dosen_ID`))) where ((`t1`.`ID` = `t4`.`Program_ID`) and (`t4`.`Kode_Jurusan` = `t3`.`kode_jurusan`) and (`t4`.`Kode_Mtk` = `t5`.`Kode_mtk`) and (`t7`.`ID` = `t4`.`Tahun_ID`) and (`t7`.`Aktif` = _latin1'Y')) group by `t4`.`Jadwal_ID` order by `t4`.`Jadwal_ID`;

-- --------------------------------------------------------

--
-- Structure for view `view_mahasiswa`
--
DROP TABLE IF EXISTS `view_mahasiswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mahasiswa` AS select `t1`.`NIM` AS `NIM`,`t1`.`level_ID` AS `level_ID`,`t1`.`username` AS `username`,`t1`.`password` AS `password`,`t1`.`Angkatan` AS `Angkatan`,`t1`.`Kurikulum_ID` AS `Kurikulum_ID`,`t1`.`identitas_ID` AS `identitas_ID`,`t1`.`Nama` AS `Nama`,`t1`.`Foto` AS `Foto`,`t1`.`StatusAwal_ID` AS `StatusAwal_ID`,`t1`.`StatusMhsw_ID` AS `StatusMhsw_ID`,`t1`.`IDProg` AS `IDProg`,`t1`.`kode_jurusan` AS `kode_jurusan`,`t1`.`Kelamin` AS `Kelamin`,`t1`.`WargaNegara` AS `WargaNegara`,`t1`.`Kebangsaan` AS `Kebangsaan`,`t1`.`TempatLahir` AS `TempatLahir`,`t1`.`TanggalLahir` AS `TanggalLahir`,`t1`.`Agama` AS `Agama`,`t1`.`StatusSipil` AS `StatusSipil`,`t1`.`Alamat` AS `Alamat`,`t1`.`Kota` AS `Kota`,`t1`.`RT` AS `RT`,`t1`.`RW` AS `RW`,`t1`.`KodePos` AS `KodePos`,`t1`.`Propinsi` AS `Propinsi`,`t1`.`Negara` AS `Negara`,`t1`.`Telepon` AS `Telepon`,`t1`.`Handphone` AS `Handphone`,`t1`.`Email` AS `Email`,`t1`.`AlamatAsal` AS `AlamatAsal`,`t1`.`KotaAsal` AS `KotaAsal`,`t1`.`RTAsal` AS `RTAsal`,`t1`.`RWAsal` AS `RWAsal`,`t1`.`KodePosAsal` AS `KodePosAsal`,`t1`.`PropinsiAsal` AS `PropinsiAsal`,`t1`.`NegaraAsal` AS `NegaraAsal`,`t1`.`NamaAyah` AS `NamaAyah`,`t1`.`AgamaAyah` AS `AgamaAyah`,`t1`.`PendidikanAyah` AS `PendidikanAyah`,`t1`.`PekerjaanAyah` AS `PekerjaanAyah`,`t1`.`HidupAyah` AS `HidupAyah`,`t1`.`NamaIbu` AS `NamaIbu`,`t1`.`AgamaIbu` AS `AgamaIbu`,`t1`.`PendidikanIbu` AS `PendidikanIbu`,`t1`.`PekerjaanIbu` AS `PekerjaanIbu`,`t1`.`HidupIbu` AS `HidupIbu`,`t1`.`AlamatOrtu` AS `AlamatOrtu`,`t1`.`KotaOrtu` AS `KotaOrtu`,`t1`.`KodePosOrtu` AS `KodePosOrtu`,`t1`.`PropinsiOrtu` AS `PropinsiOrtu`,`t1`.`NegaraOrtu` AS `NegaraOrtu`,`t1`.`TeleponOrtu` AS `TeleponOrtu`,`t1`.`HandphoneOrtu` AS `HandphoneOrtu`,`t1`.`EmailOrtu` AS `EmailOrtu`,`t1`.`AsalSekolah` AS `AsalSekolah`,`t1`.`AsalSekolah1` AS `AsalSekolah1`,`t1`.`JenisSekolah_ID` AS `JenisSekolah_ID`,`t1`.`KotaSekolah` AS `KotaSekolah`,`t1`.`JurusanSekolah` AS `JurusanSekolah`,`t1`.`NilaiSekolah` AS `NilaiSekolah`,`t1`.`TahunLulus` AS `TahunLulus`,`t1`.`aktif` AS `aktif`,`t1`.`LulusUjian` AS `LulusUjian`,`t1`.`NilaiUjian` AS `NilaiUjian`,`t1`.`GradeNilai` AS `GradeNilai`,`t1`.`TanggalLulus` AS `TanggalLulus`,`t1`.`IPK` AS `IPK`,`t1`.`TotalSKS` AS `TotalSKS`,`t2`.`nama_jurusan` AS `nama_jurusan`,`t3`.`Program_ID` AS `Program_ID`,`t1`.`PenasehatAkademik` AS `pa`,`t4`.`nama_lengkap` AS `npa`,`t4`.`Gelar` AS `Gelar` from (((`mahasiswa` `t1` join `jurusan` `t2`) join `program` `t3`) left join `dosen` `t4` on((`t1`.`PenasehatAkademik` = `t4`.`NIDN`))) where ((`t3`.`ID` = `t1`.`IDProg`) and (`t1`.`kode_jurusan` = `t2`.`kode_jurusan`) and `t1`.`Angkatan`) order by `t1`.`NIM`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
