<?php
	$idjadwal=$_GET['idjadwal'];
	$sql="select view_jadwal.*,dosen.nama_lengkap
			from  view_jadwal,dosen where 
			view_jadwal.Tahun='2014-1'
			and view_jadwal.Dosen_ID=dosen.dosen_ID
			and view_jadwal.jadwal_ID='$idjadwal' ";
	$res=mysql_query($sql) or die(mysql_error());
	$mk=mysql_fetch_object($res);
	$bobotArray=array();
?>
		
<div class="tab-content">
	<div class="tab-pane fade in active">
		<h3 class="head text-center">Hasil Kuisioner</h3>  
		<p>Nama Dosen = <?=$mk->nama_lengkap?></p>
		<p>Nama Mata Kuliah = <?=$mk->Nama_matakuliah?>   </p>
		<p>Ruang = <?=$mk->NamaRuang?> </p>
		<p>Hari/Jam =  <?=$mk->Hari?>/<?=$mk->Jam_Mulai?> </p>
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th><th>Pertanyaan</th><th>Sangat Baik</th><th>Baik</th><th>Cukup</th><th>Kurang</th><th>Jelek</th><th>IP</th>
					</tr>
				</thead>
				<tbody>
				<!-------------------------------->
					<?php
						$sql="select crm_hasil_survey.*,crm_kuisioner.isi_pertanyaan 
                                 from  crm_hasil_survey,crm_kuisioner
                                 where crm_hasil_survey.id_jadwal='$idjadwal' 
                                 and crm_hasil_survey.no_quisioner=crm_kuisioner.id_kuisioner ";
								//	 echo $sql;
						$hasil=mysql_query($sql) or die(mysql_error(). "<br>".$sql);   
						$no=0;
						$total=0;
						$jumlah_mhs=0;
					   //ini buat grafik 
					   	$totalA=0;
						$totalB=0;
					   	$totalC=0;
					  	$totalD=0;
					   	$totalE=0;
					   
                        while($mk=mysql_fetch_object($hasil)){
                        	$jumlah_mhs=$mk->A+$mk->B+$mk->C+$mk->D+$mk->E;
							$bobot=get_bobot($mk->A,$mk->B,$mk->C,$mk->D,$mk->E);
							$bobotArray[$no]=number_format($bobot,2);
							$total=$total+$bobot;
							$no++;
							
							///ini buat grafik 
							$totalA+=$mk->A;
							$totalB+=$mk->B;
							$totalC+=$mk->C;
							$totalD+=$mk->D;
							$totalE+=$mk->E;
					?>              
                    <tr>
                    	<td><?=  $mk->no_quisioner; ?></td>
                    	<td>
                        	<?=$mk->isi_pertanyaan?>                                          
						</td>
						<td>
							<?=$mk->A?>                                         
						</td>
						<td>
							<?=$mk->B?>                                          
						</td>
						<td>
							<?=$mk->C?>                                          
						</td>
						<td>
							<?=$mk->D?>                                          
						</td>
						<td>
							<?=$mk->E?>                                          
						</td>
						<td>
							<?=number_format($bobot,2);?>                                          
						</td>
                                        
					</tr>
      				<?php
					}

       //RUMUS UNTUK MEMASUKAN HILAI TOTAL */
        $grandtotal=$totalA+$totalB+$totalC+$totalD+$totalE;
		$persenA=($totalA/$grandtotal)*100;
		$persenB=($totalB/$grandtotal)*100;
		$persenC=($totalC/$grandtotal)*100;
		$persenD=($totalD/$grandtotal)*100;
		$persenE=($totalE/$grandtotal)*100;
		
                        ?>                         
     <!-------------------------------->                                 
						<tr>
							<td></td><td colspan='1'>
								<h5 style='text-align:center;'><strong>IPAD</strong></h5>                                       
							</td>
							<td colspan='6'>
								<h5 style='text-align:center;'><strong><?php
									echo number_format($total/$no,2);
											?>
							</strong></h5></td>                                      
						</tr>                                      
				</tbody>
			</table>
                            
     
   			<!--------------------------------------HIGHCART ---------------------------------->
   			<script type="text/javascript" src="assets/js/jquery.js"></script>
			<script src="assets/hg/highcharts.js"></script>
			<script type="text/javascript">
				$(function(){	
					var chart1; // globally available
					chart1 = new Highcharts.Chart({
         				chart: {
            				renderTo: 'barchart',
            				type: 'column'
         				},   
         				title: {
            			text: 'Grafik IP Dosen '
         				},
         				xAxis: {
            				categories: ['no Soal']
         				},
         				yAxis: {
            				title: {
               					text: 'index'
            				}
         				},
              			series:             
            				[
                   				<?php
                   			for($x = 0; $x <count($bobotArray); $x++) {
    							$no=$x+1;
								echo "   {name: '$no', data: [$bobotArray[$x]]},";
    						}?>                          
             			]
      				});
   				}); 
			</script>
			<script>
				$(document).ready(function(){
      				$('body').append('<div id="toTop" class="btn btn-danger"><span class="glyphicon glyphicon-chevron-up"></span></div>');
    				$(window).scroll(function () {
						if ($(this).scrollTop() != 0) {
							$('#toTop').fadeIn();
						} else {
							$('#toTop').fadeOut();
						}
					}); 
    				$('#toTop').click(function(){
        				$("html, body").animate({ scrollTop: 0 }, 600);
        				return false;
    				});
				});
			</script>
		<div id="barchart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
		</div>
		<button onclick="window.history.back()" class='btn btn-primary'><i class='icon-arrow-left'></i>Kembali</button>	
	</div>
</div>