<?php 
	$NIDN = $_GET['id'];
	if (empty($NIDN)) {
		$NIDN = $_SESSION['username'];
	}
?>

<div class="tab-content">
	<div class="tab-pane fade in active">
		<h3 class="head text-center">Biodata Dosen</h3>  
		<?php 
			$query = "select dosen.* from dosen where dosen.NIDN='$NIDN'";
			$result = mysql_query($query) or die(mysql_error());
			$rows = mysql_fetch_object($result);
		?>
		<div class="table-responsive">
			<table class="table table-hover  table-striped">
				<tbody>
					<tr>		
						<td width="200px"><b>Nama</b></td>
						<td>:</td>
						<td><? echo $rows -> nama_lengkap; ?></td>
						<td rowspan='9'><img src='upload/foto/foto.jpg' width='200px' height='200px'/></td>
					</tr>		
					<tr>		
						<td width="200px"><b>NIDN</b></td>
						<td>:</td>
						<td><? echo $rows -> NIDN; ?></td>
					</tr>
					<tr>		
						<td width="200px"><b>TTL</b></td>
						<td>:</td>
						<td><? echo $rows -> TempatLahir; ?>/
							<? echo tampil_tanggal($rows -> TanggalLahir); ?></td>			
					</tr>
					<tr>		
						<td width="200px"><b>Alamat</b></td>
						<td>:</td>
						<td><? echo $rows -> Alamat; ?></td>			
					</tr>
					<tr>		
						<td width="200px"><b>Agama</b></td>
						<td>:</td>
						<td><? echo $rows ->Agama ?></td>			
					</tr>
			</table>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"style='text-align: center;margin-bottom:0px;'>Jadwal Matakuliah</h3>
			</div>
			<div class="panel-body">
				<div class='table-responsive'>
					<table class="table table-hover table-condensed table-striped">
						<thead>
							<tr>
								<th>No</th><th>Matakuliah</th>
								<th> Ruang </th><th> Hari</th>
								<th> Jam</th><th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						<!-------------------------------->
							<?php
							//$NIDN=$_SESSION['username'];
								$sql="select * from  view_jadwal,dosen
										where 
										view_jadwal.dosen_ID=dosen.dosen_ID
										and view_jadwal.Tahun='2014-1'
										and dosen.NIDN='$NIDN'  ";
								$hasil=mysql_query($sql) or die(mysql_error(). "<br>".$sql);   
								$no=1;
								while($mk=mysql_fetch_object($hasil)){
							?>              
							<tr>
								<td><?= $posisi + $no; ?></td>
								<td>
									<?=$mk->Nama_matakuliah?>                                          
								</td>
								<td>
									<?=$mk->NamaRuang?>                                         
								</td>
								<td>
									<?=$mk->Hari?>                                          
								</td>
								<td>
									<?=$mk->Jam_Mulai?>                                          
								</td>
								<td>
									<a href='index.php?m=matakuliah&pg=matakuliah_peserta&id=<?=$mk->Jadwal_ID?>' 
										class="btn btn-info" data-toggle="tooltip" title="mahasiswa">
										<i class='glyphicon glyphicon-user'></i></a>&nbsp;
									<?php  if(cek_status_polling($mk->Jadwal_ID)>0): ?>
										<a href='index.php?m=crm_polling&pg=crm_polling_view&idjadwal=<?=$mk->Jadwal_ID?>' 
											class="btn btn-info" data-toggle="tooltip" title="hasil kuisioner"><i class='glyphicon glyphicon-stats'></i></a>&nbsp;
									<?php else: ?>
										poling belum ada
									<?php endif;?>
								</td>
							</tr>
							<?php
								$no++;
							}
							?>                         
							<!-------------------------------->                                 
							<tr></tr>
						</tbody>
					</table>
				</div> <!--div table responsive-->
			</div>
		</div>
		<button onclick="window.history.back()" class='btn btn-primary'>
			<i class='icon-arrow-left'></i>Kembali</button>    
	</div>
</div>
