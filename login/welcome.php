<div class="tab-content">
	<div class="tab-pane fade in active">
		<div class="box">
			<div id="carousel-example-generic" class="carousel slide">
            <!-- Wrapper for slides -->
            	<div class="carousel-inner">
                	<div class="item active">
                    	<img class="img-responsive img-full" src="assets/img/lab1.jpg"alt="">
                    </div>
                    <div class="item">
                    	<img class="img-responsive img-full" src="assets/img/musholla.jpg" alt="">
                    </div>
                    <div class="item">
                    	<img class="img-responsive img-full" src="assets/img/depan.jpg" alt="">
                    </div>
                 </div>
					<!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        	<span class="glyphicon icon-prev"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon icon-next"></span>
                        </a>
			</div>
		</div>
	</div>
</div>