<?php
	$aksi = 'tambah';
?>    

<div class="tab-content">
	<div class="tab-pane fade in active">
		<h3 class="head text-center">Form Saran</h3>
			<form  id='form1' method="POST" 
				action='crm_evaluasi/crm_evaluasi_action.php'
				class="form-horizontal" role="form"
				enctype="multipart/form-data">
				<input type='hidden' name='nim' value='<?=$_SESSION['username']?>'>
  				<div class="form-group">
    				<label for="inputEmail3" class="col-sm-3 control-label">Kategori
    				</label>
    				<div class="col-sm-9">
     					<select name='id_crm_kategori' class='form-control required'>
     						<?php
     							combo_crm_kategori2(null);
     						?>
     					</select>
    				</div>
  				</div>
   
    			<div class="form-group">
    				<label for="inputEmail3" class="col-sm-3 control-label">Saran dan kritik
    				</label>
    				<div class="col-sm-9">
     					<textarea name='isi_evaluasi' class='form-control required' name='isi_evaluasi'></textarea>
    				</div>
  				</div>
 
  				<div class="form-group">
    				<div class="col-sm-offset-3 col-sm-8">
      					<button type="submit" class="btn btn-primary" name='aksi' value='<?=$aksi?>'>Kirim</button>
      					<a href='index.php?m=crm_evaluasi&pg=crm_evaluasi_mview' class='btn btn-danger'>Cancel</a>
    				</div>
  				</div>
			</form>
	</div><!--/panel content-->
</div><!--/panel-->
                    

                