<?php
	error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Login CRM </title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="assets/css/login.css" rel="stylesheet">

		<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<style type="text/css">
		/* cara menggganti bacgkround pattern */			
			body{
			background-image:url('assets/img/shattered.png');
			background-repeat:repeat;}			
		</style>
	</head>	

	<!-- HTML code from Bootply.com editor -->

	<body>
		<div class="container">
			<div class="row">
        		<div class="col-sm-6 col-md-4 col-md-offset-4" style="margin-top: 50px"> 
        			<img src="assets/img/akakom.ac.id-logo.png"
                    class="img-responsive" alt="">
                </div>
            </div>
    		<div class="row">
            	<div class="col-sm-6 col-md-4 col-md-offset-4">            
            	<div class="account-wall">                
                	<form class="form-signin" method='POST' action='login/login_action.php'>
                		<input type="text"  name='username' class="form-control" placeholder="Username" required autofocus> <p></p>
                		<input type="password"  name='password' class="form-control" placeholder="Password" required>
                		<!--<select  name='level' class="form-control" >
  						<option value='pengelola'>Pengelola</option>
  						<option value='dosen'>Dosen</option>  			
  						<option value='mahasiswa'>Mahasiswa</option>
						</select><p></p>-->
                		<button class="btn btn-lg btn-danger btn-block" type="submit">
                    		Sign in
                    	</button>                
                	</form>
                  	<p> 
                		<?php
                			if($_GET['status']){
                    			echo "Login gagal!";
                			}
                		?>
                	</p>
            	</div>
          	</div>
        </div>
    </div>

	</body>
</html>